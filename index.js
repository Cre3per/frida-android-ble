const config = require('./config');

const Frida = require('frida');
const FridaInject = require('frida-inject');
const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
  completer: (line) => main.completer(line),
});

class Main
{
  async _prompt(script, prompt)
  {
    rl.question(`${prompt}> `, (input) =>
    {
      if (input === 'q')
      {
        process.exit(0);
      }
      else
      {
        script.post(input);
      }
    });
  }

  /**
   * 
   */
  _stringifyConfig()
  {
    // No `for of` support.
    for (let i in Object.keys(config.known_data))
    {
      let characteristic = Object.keys(config.known_data)[i];

      for (let j in config.known_data[characteristic])
      {
        let data = config.known_data[characteristic][j];

        data.match = data.match.toString();
      }
    }
  }

  /**
   * Injects the device code.
   * @return {Promise<(undefined | Script))>}
   */
  async _inject()
  {
    // Injection timeout in milliseconds
    const TIMEOUT = 3000;

    this._stringifyConfig();

    let device = await (config.device ? Frida.getDevice(config.device) : Frida.getUsbDevice());

    return new Promise((resolve, reject) =>
    {
      let returned = false;

      setTimeout(() =>
      {
        if (!returned)
        {
          returned = true;
          resolve();
        }
      }, TIMEOUT);

      FridaInject({
        debug: false,
        device: device,
        name: process.argv[2],

        scripts: [`${__dirname}/build/index.js`],

        onLoad: (script) =>
        {
          script.post({
            type: 'config',
            payload: config,
          });

          if (!returned)
          {
            returned = true;
            resolve(script);
          }
        }
      }).catch((reason) =>
      {
        console.error('injection failed');
        console.error(reason.stack);
      });
    });
  }

  /**
   * Verifies command line arguments.
   * @return {boolean} True if all arguments are valid.
   */
  _verifyArgs()
  {
    return (process.argv.length === 3);
  }

  completer(line)
  {
    return [
      this._aliases.filter(el => el.startsWith(line)),
      line
    ];
  }

  async _main()
  {
    let script = await this._inject();

    if (script)
    {
      script.message.connect((message) =>
      {
        if (message.type === 'send')
        {
          if (message.payload.startsWith('aliases:'))
          {
            this._aliases = message.payload.substr('aliases:'.length).split(',');
          }
          else
          {
            this._prompt(script, message.payload);
          }
        }
        else if (message.type === 'error')
        {
          // I don't think this can happen. Something is eating exceptions.
          console.error(message.stack);
        }
      });
    }
    else
    {
      console.log('injection timed out');
      process.exit(1);
    }
  }

  constructor()
  {
    /**
     * @type {string[]}
     */
    this._aliases = [];

    if (!this._verifyArgs())
    {
      console.log('usage: frida-android-ble [process]');
      process.exit(1);
    }

    this._main().catch((reason) =>
    {
      console.error('unhandled exception');
      console.error(reason);
    });
  }
}

let main = new Main();