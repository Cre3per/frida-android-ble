# Upload and run frida-server from the local directory.
# Script can be cancelled when the upload is complete.

adb -s e3da6916 push ./frida-server /data/local/tmp/
adb -s e3da6916 shell "su -c chmod +x /data/local/tmp/frida-server"
adb -s e3da6916 shell "su -c /data/local/tmp/frida-server"