let config = {
  // id of the android device (Use `adb devices` to list ids). Set to null to
  // if there is only one remote device.
  device: null,

  // Use colored terminal output.
  color: true,

  // How many entries the data logs can have each.
  history_size: 512,

  // Enable/disable callbacks.
  // More classes and function can be added, but no rich output can be provided.
  // A callback is added for every overload of the listed functions.
  // Callbacks can be set the 'generic' to force the use of a generic logging
  // callback even if a rich callback is available.
  callbacks: {
    'android.bluetooth.IBluetoothGatt$Stub$Proxy': {
      'readCharacteristic': true,
      'writeCharacteristic': true,
    },
    'android.bluetooth.BluetoothGatt': {
      'getService': true,
      'writeCharacteristic': true,
      'writeDescriptor': true,
    },
    'android.bluetooth.BluetoothGattCallback': {
      'onCharacteristicChanged': true,
    },
    'android.bluetooth.BluetoothGattCharacteristic': {
      'getValue': true,
    },
  },

  // Log the caller if BluetoothGattCharacteristic.getValue is called by
  // BluetoothGattCallback.onCharacteristicChanged.
  debug_on_characteristic_changed: false,

  // Map from known mac addresses to device names.
  // Mac addresses will be replaced by their device name.
  known_devices: {
    // 'MA:CA:DD:RE:SS': 'Friendly name',
  },

  // Map from service uuids to service names.
  // Service uuids are replaced by their names.
  known_services: {
    // 'service-uuid': 'Friendly name',
  },

  // Map from characteristic uuids to characteristic names.
  // Characteristic uuids are replaced by their names.
  known_characteristics: {
    // 'characteristic-name': 'Friendly name',
  },

  // Map from characteristic uuids to arrays of { match: RegExp, name: string }.
  // Logged data is prefixed by its friendly name.
  // Example:
  // known_data: {
  //   '6e400002-b5a3-f393-e0a9-e50e24dcca9e': [
  //     {
  //       match: /^ab 00 [0-f]{2} ff 23/,
  //       name: 'CMD_FACTORY_RESET'
  //     },
  //   ],
  // }
  known_data: {
    // 'characteristic-uui': [
    //   {
    //     match: /regex/,
    //     name: 'Friendly name'
    //   },
    // ],
  }
};

module.exports = config;