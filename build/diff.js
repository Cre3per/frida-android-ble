"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Diff = /** @class */ (function () {
    function Diff() {
    }
    /**
     * @return An array with a true entry wherever the inputs match.
     */
    Diff.bytes = function (a, b) {
        var short = a.length;
        var long = b.length;
        if (short > long) {
            var tmp = short;
            short = long;
            long = tmp;
        }
        var result = (new Array(long)).fill(false);
        for (var i = 0; i < short; ++i) {
            result[i] = (a[i] === b[i]);
        }
        return result;
    };
    return Diff;
}());
exports.Diff = Diff;
//# sourceMappingURL=diff.js.map