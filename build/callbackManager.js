"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var history_js_1 = require("./history.js");
var logger_js_1 = require("./logger.js");
var human_js_1 = require("./human.js");
var config_js_1 = require("./config.js");
var formatter_js_1 = require("./formatter.js");
/**
 * {@link https://github.com/pandalion98/SPay-inner-workings/blob/master/boot.oat_files/arm64/dex/out0/android/bluetooth/IBluetoothGatt.java}
 */
var IBluetoothGatt$Stub$Proxy = /** @class */ (function () {
    /**
     *
     */
    function IBluetoothGatt$Stub$Proxy() {
        this._className = 'IBluetoothGatt$Stub$Proxy';
    }
    /**
     * @param {primitive number} clientIf
     * @param {java.lang.String} address
     * @param {primitive number} srvcType
     * @param {primitive number} srvcInstance
     * @param unknown srvcInstanceId, srvcId, charInstanceId, charId, writeType, authReq
     * @param {primitive byte[]} clientIf
     */
    IBluetoothGatt$Stub$Proxy.prototype.writeCharacteristic = function (context, clientIf, address, srvcType, srvcInstance, unknown, value) {
        var characteristicValue = value;
        var humanDevice = new human_js_1.Device(address);
        var humanData = new human_js_1.Data(undefined, characteristicValue);
        var historyId = history_js_1.historyManager.pushBytes(history_js_1.HistoryType.WRITE, formatter_js_1.Formatter.toJSArray(characteristicValue));
        logger_js_1.Logger.logFunctionCall(this._className + ".writeCharacteristic", {
            'clientIf': clientIf,
            'address': humanDevice.getDisplayName(),
            'srvcType': srvcType,
            'srvcInstance': srvcInstance,
            'a5': unknown,
            'data': "[" + history_js_1.historyManager.getHistoryName(history_js_1.HistoryType.WRITE) + " " + historyId + "] " + humanData.getDisplayName(),
        });
    };
    return IBluetoothGatt$Stub$Proxy;
}());
var BluetoothGatt = /** @class */ (function () {
    /**
     *
     */
    function BluetoothGatt() {
        this._className = 'BluetoothGatt';
    }
    /**
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     */
    BluetoothGatt.prototype.writeCharacteristic = function (context, characteristic) {
        exports.callbacks.enabled = false;
        var characteristicValue = characteristic.getValue();
        exports.callbacks.enabled = true;
        var humanDevice = new human_js_1.Device(context.this.getDevice());
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic.getService());
        var humanData = new human_js_1.Data(characteristic, characteristicValue);
        var historyId = history_js_1.historyManager.pushBytes(history_js_1.HistoryType.WRITE, formatter_js_1.Formatter.toJSArray(characteristicValue));
        logger_js_1.Logger.logFunctionCall(this._className + ".writeCharacteristic", {
            'device': humanDevice.getDisplayName(),
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': "[" + history_js_1.historyManager.getHistoryName(history_js_1.HistoryType.WRITE) + " " + historyId + "] " + humanData.getDisplayName(),
        });
    };
    /**
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     */
    BluetoothGatt.prototype.readCharacteristic = function (context, characteristic) {
        exports.callbacks.enabled = false;
        var characteristicValue = characteristic.getValue();
        exports.callbacks.enabled = true;
        var humanDevice = new human_js_1.Device(context.this.getDevice());
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic.getService());
        var humanData = new human_js_1.Data(characteristic, characteristicValue);
        logger_js_1.Logger.logFunctionCall(this._className + ".readCharacteristic", {
            'device': humanDevice.getDisplayName(),
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': humanData.getDisplayName(),
        });
    };
    /**
     * @param {android.bluetooth.BluetoothGattDescriptor} descriptor
     */
    BluetoothGatt.prototype.writeDescriptor = function (context, descriptor) {
        var device = context.this.getDevice();
        var characteristic = descriptor.getCharacteristic();
        var humanDevice = new human_js_1.Device(device);
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic ? characteristic.getService() : null);
        var humanData = new human_js_1.Data(characteristic, descriptor.getValue());
        var historyId = history_js_1.historyManager.pushBytes(history_js_1.HistoryType.WRITE, formatter_js_1.Formatter.toJSArray(descriptor.getValue()));
        logger_js_1.Logger.logFunctionCall(this._className + ".writeDescriptor", {
            'device': humanDevice.getDisplayName(),
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': "[" + history_js_1.historyManager.getHistoryName(history_js_1.HistoryType.WRITE) + " " + historyId + "] " + humanData.getDisplayName(),
        });
    };
    return BluetoothGatt;
}());
var BluetoothGattCallback = /** @class */ (function () {
    function BluetoothGattCallback() {
        this._className = 'BluetoothGattCallback';
    }
    /**
     * @param {android.bluetooth.BluetoothGatt} gatt
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     */
    BluetoothGattCallback.prototype.onCharacteristicChanged = function (context, gatt, characteristic) {
        exports.callbacks.enabled = false;
        var value = characteristic.getValue();
        exports.callbacks.enabled = true;
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic.getService());
        var humanData = new human_js_1.Data(characteristic, value);
        var historyId = history_js_1.historyManager.pushBytes(history_js_1.HistoryType.READ, formatter_js_1.Formatter.toJSArray(value));
        logger_js_1.Logger.logFunctionCall(this._className + ".onCharacteristicChanged", {
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': "[" + history_js_1.historyManager.getHistoryName(history_js_1.HistoryType.READ) + " " + historyId + "] " + humanData.getDisplayName(),
        });
    };
    return BluetoothGattCallback;
}());
var BluetoothGattCharacteristic = /** @class */ (function () {
    function BluetoothGattCharacteristic() {
        this._className = 'BluetoothGattCharacteristic';
    }
    /**
     *
     */
    BluetoothGattCharacteristic.prototype.getValue = function (context) {
        var _this_1 = this;
        var humanCharacteristic = new human_js_1.Characteristic(context.this);
        var humanService = new human_js_1.Service(context.this.getService());
        var humanData = new human_js_1.Data(context.this, context.retval);
        var historyId = history_js_1.historyManager.pushBytes(history_js_1.HistoryType.READ, formatter_js_1.Formatter.toJSArray(context.retval));
        if (config_js_1.config.debug_on_characteristic_changed) {
            Java.perform(function () {
                // I'm sure there's a nicer way, but this is for debugging only and I
                // don't like Java.
                var trace = Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new());
                var lines = trace.split('\n');
                if (lines.length >= 3) {
                    if (lines[2].includes('.onCharacteristicChanged(')) {
                        console.log(_this_1._className + ".getValue called", lines[2].trim());
                    }
                }
            });
        }
        logger_js_1.Logger.logFunctionCall(this._className + ".getValue", {
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': "[" + history_js_1.historyManager.getHistoryName(history_js_1.HistoryType.READ) + " " + historyId + "] " + humanData.getDisplayName(),
        });
    };
    return BluetoothGattCharacteristic;
}());
var CallbackManager = /** @class */ (function () {
    /**
     *
     */
    function CallbackManager() {
        var _this_1 = this;
        /**
         * Maps function addresses to the original function implementation.
         */
        this._originalImplementations = {};
        /**
         * Enable hooks. Used to prevent recursive and internal calls from within
         * hooks.
         */
        this.enabled = true;
        this['android.bluetooth.IBluetoothGatt$Stub$Proxy'] = new IBluetoothGatt$Stub$Proxy();
        this['android.bluetooth.BluetoothGatt'] = new BluetoothGatt();
        this['android.bluetooth.BluetoothGattCallback'] = new BluetoothGattCallback();
        this['android.bluetooth.BluetoothGattCharacteristic'] = new BluetoothGattCharacteristic();
        config_js_1.ConfigManager.attachChangeListener('callbacks', function (keyChain, oldValue, newValue) {
            // We're safe to assume that @keyChain refers to a valid callback.
            var className = keyChain[1];
            var functionName = keyChain[keyChain.length - 1];
            _this_1.toggleCallback(className, functionName);
        });
    }
    /**
     *
     */
    CallbackManager.prototype._getFunctionId = function (javaFunction) {
        return javaFunction.handle.toString();
    };
    /**
     * Returns a list of alternative class names for the class @className.
     * Callbacks are installed under the @className name, even when they actually
     * hook a class of a different name.
     */
    CallbackManager.prototype._findAlternativeClasses = function (className) {
        switch (className) {
            case 'android.bluetooth.BluetoothGattCallback':
                return [
                    'no.nordicsemi.android.ble.MainThreadBluetoothGattCallback'
                ];
            default:
                return [];
        }
    };
    /**
     * @return {MethodDispatcher}
     */
    CallbackManager.prototype._getJavaFunctionByName = function (className, functionName) {
        var javaClass;
        try {
            javaClass = Java.use(className);
        }
        catch (_a) {
            return undefined;
        }
        return javaClass[functionName];
    };
    /**
     *
     */
    CallbackManager.prototype._installCallback = function (javaFunction, callback) {
        var _this = this;
        javaFunction.implementation = function () {
            var _a;
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var result = (_a = javaFunction.callWrapped).call.apply(_a, [this].concat(args));
            if (_this.enabled) {
                callback.apply(void 0, [{
                        this: this,
                        retval: result,
                    }].concat(args));
            }
            return result;
        };
    };
    /**
     *
     */
    CallbackManager.prototype._enableOverloadedCallback = function (javaFunction, callbackClassName, functionName, forceGeneric) {
        var _this_1 = this;
        var functionId = this._getFunctionId(javaFunction);
        this._originalImplementations[functionId] = javaFunction.implementation;
        var callback;
        if (!forceGeneric && this[callbackClassName] && this[callbackClassName][functionName]) {
            callback = function () {
                var _a;
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                (_a = _this_1[callbackClassName])[functionName].apply(_a, args);
            };
        }
        else // No rich callback available or desired.
         {
            callback = function (context) {
                var args = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    args[_i - 1] = arguments[_i];
                }
                _this_1.genericCallback.apply(_this_1, [callbackClassName + "." + functionName, context].concat(args));
            };
        }
        this._installCallback(javaFunction, callback);
    };
    /**
     * Enables a hook.
     */
    CallbackManager.prototype._enableCallback = function (className, functionName, callbackClassName, forceGeneric) {
        var javaFunction = this._getJavaFunctionByName(className, functionName);
        if (!javaFunction) {
            return;
        }
        for (var _i = 0, _a = javaFunction.overloads; _i < _a.length; _i++) {
            var overload = _a[_i];
            // javaFunction(...) doesn't work, we need a wrapper.
            overload.callWrapped = function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return this[functionName].apply(this, args);
            };
            this._enableOverloadedCallback(overload, callbackClassName, functionName, forceGeneric);
        }
    };
    /**
     *
     */
    CallbackManager.prototype._disableOverloadedCallback = function (javaFunction) {
        var functionId = this._getFunctionId(javaFunction);
        var originalImplementation = this._originalImplementations[functionId];
        javaFunction.implementation = originalImplementation;
        delete this._originalImplementations[functionId];
    };
    /**
     * Enables a hook and all of its alternative hooks.
     * @param className Class parenting the function.
     * @param functionName Function to be hooked.
     * @param forceGeneric Use the generic callback, even if a rich one exists.
     */
    CallbackManager.prototype.enableCallback = function (className, functionName, forceGeneric) {
        var _this_1 = this;
        if (forceGeneric === void 0) { forceGeneric = false; }
        Java.perform(function () {
            var classNames = [className].concat(_this_1._findAlternativeClasses(className));
            for (var _i = 0, classNames_1 = classNames; _i < classNames_1.length; _i++) {
                var _className = classNames_1[_i];
                _this_1._enableCallback(_className, functionName, className, forceGeneric);
            }
        });
    };
    /**
     * Disables a hook.
     */
    CallbackManager.prototype.disableCallback = function (className, functionName) {
        var _this_1 = this;
        Java.perform(function () {
            var javaFunction = _this_1._getJavaFunctionByName(className, functionName);
            if (!javaFunction) {
                console.log(className + "." + functionName + " doesn't exist");
                return;
            }
            for (var _i = 0, _a = javaFunction.overloads; _i < _a.length; _i++) {
                var overload = _a[_i];
                _this_1._disableOverloadedCallback(overload);
            }
        });
    };
    /**
     * Toggles a hook.
     */
    CallbackManager.prototype.toggleCallback = function (className, functionName) {
        var _this_1 = this;
        Java.perform(function () {
            var javaFunction = _this_1._getJavaFunctionByName(className, functionName);
            if (javaFunction === undefined) {
                console.log(className + "." + functionName + " doesn't exist.");
                return;
            }
            var functionId = _this_1._originalImplementations[_this_1._getFunctionId(javaFunction)];
            if (functionId === undefined) {
                _this_1.enableCallback(className, functionName);
            }
            else {
                _this_1.disableCallback(className, functionName);
            }
        });
    };
    /**
     * Installs Java hooks.
     */
    CallbackManager.prototype.installCallbacks = function () {
        for (var className in config_js_1.config.callbacks) {
            for (var functionName in config_js_1.config.callbacks[className]) {
                if (config_js_1.config.callbacks[className][functionName]) {
                    this.enableCallback(className, functionName, config_js_1.config.callbacks[className][functionName] === 'generic');
                }
            }
        }
    };
    /**
     * Used for functions without a custom callback implementation.
     */
    CallbackManager.prototype.genericCallback = function (name, context) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var javaObject = Java.use('java.lang.Object');
        var logArgs = {};
        var i = 0;
        for (var _a = 0, args_1 = args; _a < args_1.length; _a++) {
            var arg = args_1[_a];
            var argString = void 0;
            try {
                argString = Java.cast(arg, javaObject).toString();
            }
            catch (_b) {
                argString = "" + arg;
            }
            // HACKHACK: Key-Value order is undefined in JS. We could fix this by
            // HACKHACK: rewriting logFunctionCall to take an array rather than an
            // HACKHACK: object.
            logArgs["a" + i++] = argString;
        }
        logger_js_1.Logger.logFunctionCall(name, logArgs);
    };
    return CallbackManager;
}());
exports.callbacks = new CallbackManager();
//# sourceMappingURL=callbackManager.js.map