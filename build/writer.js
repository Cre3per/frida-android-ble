"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Writer = /** @class */ (function () {
    function Writer() {
        this._gatts = [];
    }
    /**
     *
     */
    Writer.prototype._collectInstances = function (className) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                result = [];
                return [2 /*return*/, new Promise(function (resolve) {
                        Java.perform(function () {
                            Java.choose(className, {
                                onMatch: function (instance) {
                                    result.push({
                                        id: result.length,
                                        handle: Java.retain(instance),
                                    });
                                },
                                onComplete: function () {
                                    resolve(result);
                                }
                            });
                        });
                    })];
            });
        });
    };
    /**
     *
     */
    Writer.prototype._collectGatts = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._collectInstances('android.bluetooth.BluetoothGatt')];
            });
        });
    };
    /**
     * @return The characteristic's id.
     */
    Writer.prototype._getCharacteristicByUuid = function (uuid) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        Java.perform(function () {
                            for (var _i = 0, _a = _this._gatts; _i < _a.length; _i++) {
                                var gatt = _a[_i];
                                for (var _b = 0, _c = gatt.services; _b < _c.length; _b++) {
                                    var service = _c[_b];
                                    for (var _d = 0, _e = service.characteristics; _d < _e.length; _d++) {
                                        var characteristic = _e[_d];
                                        if (characteristic.handle.getUuid().toString() === uuid) {
                                            resolve(characteristic.id);
                                        }
                                    }
                                }
                            }
                            resolve(undefined);
                        });
                    })];
            });
        });
    };
    /**
     *
     */
    Writer.prototype._getCharacteristicPath = function (id) {
        for (var _i = 0, _a = this._gatts; _i < _a.length; _i++) {
            var gatt = _a[_i];
            for (var _b = 0, _c = gatt.services; _b < _c.length; _b++) {
                var service = _c[_b];
                for (var _d = 0, _e = service.characteristics; _d < _e.length; _d++) {
                    var characteristic = _e[_d];
                    if (characteristic.id === id) {
                        return {
                            gatt: gatt,
                            service: service,
                            characteristic: characteristic
                        };
                    }
                }
            }
        }
        return undefined;
    };
    /**
     * {@link update} has to be called first.
     */
    Writer.prototype.getGatts = function () {
        return this._gatts;
    };
    /**
     * @param characteristic Characteristic id or uuid.
     */
    Writer.prototype.writeCharacteristic = function (characteristicId, value) {
        return __awaiter(this, void 0, void 0, function () {
            var internalId, characteristicPath;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this._gatts.length === 0)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.update()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!(typeof (characteristicId) === 'string')) return [3 /*break*/, 4];
                        return [4 /*yield*/, this._getCharacteristicByUuid(characteristicId)];
                    case 3:
                        internalId = _a.sent();
                        if (internalId === undefined) {
                            console.log("unknown uuid");
                            return [2 /*return*/, false];
                        }
                        else {
                            characteristicId = internalId;
                        }
                        _a.label = 4;
                    case 4:
                        characteristicPath = this._getCharacteristicPath(characteristicId);
                        if (characteristicPath) {
                            return [2 /*return*/, new Promise(function (resolve) {
                                    Java.perform(function () {
                                        characteristicPath = characteristicPath;
                                        var characteristic = characteristicPath.characteristic;
                                        if (characteristic.handle.setValue(Java.array('byte', value))) {
                                            resolve(characteristicPath.gatt.handle.writeCharacteristic(characteristic.handle));
                                        }
                                        else {
                                            console.log('characteristic.setValue failed');
                                            resolve(false);
                                        }
                                    });
                                })];
                        }
                        else {
                            console.warn("called writeCharacteristic on an invalid device or characteristic");
                            return [2 /*return*/, false];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Gets all active characteristics and stores them locally.
     * Requires that android.bluetooth.BluetoothGatt.discoverServices has already
     * been called on the gatt instances.
     * @return The found characteristics.
     */
    Writer.prototype.update = function () {
        return __awaiter(this, void 0, void 0, function () {
            var nextGattId, nextServiceId, nextCharacteristicId, gatts;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // should we Java.retain in here?
                        this._gatts = [];
                        nextGattId = 0;
                        nextServiceId = 0;
                        nextCharacteristicId = 0;
                        return [4 /*yield*/, this._collectGatts()];
                    case 1:
                        gatts = _a.sent();
                        return [2 /*return*/, new Promise(function (resolve) {
                                Java.perform(function () {
                                    for (var _i = 0, gatts_1 = gatts; _i < gatts_1.length; _i++) {
                                        var gatt = gatts_1[_i];
                                        _this._gatts.push({
                                            id: nextGattId++,
                                            handle: gatt.handle,
                                            services: [],
                                        });
                                        var mGatt = _this._gatts[_this._gatts.length - 1];
                                        // The cast overload of @toArray doesn't work.
                                        var services = gatt.handle.getServices().toArray();
                                        var BluetoothGattService = Java.use('android.bluetooth.BluetoothGattService');
                                        var BluetoothGattCharacteristic = Java.use('android.bluetooth.BluetoothGattCharacteristic');
                                        for (var _a = 0, services_1 = services; _a < services_1.length; _a++) {
                                            var service = services_1[_a];
                                            var javaService = Java.cast(service, BluetoothGattService);
                                            mGatt.services.push({
                                                id: nextServiceId++,
                                                handle: javaService,
                                                characteristics: [],
                                            });
                                            var mService = mGatt.services[mGatt.services.length - 1];
                                            var characteristics = javaService.getCharacteristics().toArray();
                                            for (var _b = 0, characteristics_1 = characteristics; _b < characteristics_1.length; _b++) {
                                                var characteristic = characteristics_1[_b];
                                                mService.characteristics.push({
                                                    id: nextCharacteristicId++,
                                                    handle: Java.cast(characteristic, BluetoothGattCharacteristic),
                                                });
                                            }
                                        }
                                    }
                                    resolve();
                                });
                            })];
                }
            });
        });
    };
    return Writer;
}());
exports.Writer = Writer;
//# sourceMappingURL=writer.js.map