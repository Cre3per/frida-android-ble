"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var config_js_1 = require("./config.js");
var Color = /** @class */ (function () {
    function Color() {
    }
    /**
     * @link{https://stackoverflow.com/a/33206814/9364954}
     */
    Color.convert = function (str, effects) {
        if (config_js_1.config.color) {
            var result = "\u001B[";
            for (var i = 0; i < effects.length; ++i) {
                result += effects[i];
                if (i !== (effects.length - 1)) {
                    result += ';';
                }
            }
            result += 'm';
            result += str;
            result += '\x1b[0m';
            return result;
        }
        else {
            return str;
        }
    };
    Color.red = function (str) {
        return this.convert(str, [30 + 1]);
    };
    Color.RED = function (str) {
        return this.convert(str, [40 + 1]);
    };
    Color.yellow = function (str) {
        return this.convert(str, [30 + 3]);
    };
    Color.YELLOW = function (str) {
        return this.convert(str, [40 + 3]);
    };
    Color.purple = function (str) {
        return this.convert(str, [30 + 5]);
    };
    Color.PURPLE = function (str) {
        return this.convert(str, [40 + 5]);
    };
    return Color;
}());
exports.Color = Color;
//# sourceMappingURL=color.js.map