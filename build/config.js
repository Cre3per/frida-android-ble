"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = {};
var ConfigManager = /** @class */ (function () {
    function ConfigManager() {
    }
    /**
     * Used to wait until the config has been loaded.
     */
    ConfigManager._configLoaded = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (Object.keys(exports.config).length > 0) {
                resolve();
            }
            else {
                _this._loadedCallbacks.push(resolve);
            }
        });
    };
    /**
     * Converts a key chain string to a key chain array.
     * Key chain strings are separated by dots and brackets.
     *
     * eg.
     * callbacks[android.bluetooth.BluetoothGatt].writeCharacteristic
     * turns into
     * ['callbacks', 'android.bluetooth.BluetoothGatt', 'writeCharacteristic']
     */
    ConfigManager._keyChainFromString = function (keyChainString) {
        var keyChain = [];
        var bracket = false;
        var key = '';
        var fnKeyEnd = function () {
            if (key.length > 0) {
                keyChain.push(key);
                key = '';
            }
            else {
                throw new Error("keys cannot be empty, in " + keyChainString);
            }
        };
        for (var _i = 0, keyChainString_1 = keyChainString; _i < keyChainString_1.length; _i++) {
            var c = keyChainString_1[_i];
            switch (c) {
                case ']':
                    if (bracket) {
                        bracket = false;
                    }
                    else {
                        throw new Error("bracket closed but not opened in " + keyChainString);
                    }
                    break;
                case '.':
                    if (bracket) {
                        key += c;
                    }
                    else {
                        fnKeyEnd();
                    }
                    break;
                case '[':
                    bracket = true;
                    fnKeyEnd();
                    break;
                default:
                    key += c;
                    break;
            }
        }
        if (bracket) {
            throw new Error("bracket opened but not closed in " + keyChainString);
        }
        if (key.length > 0) {
            keyChain.push(key);
        }
        else {
            throw new Error(keyChainString + " cannot end in a period");
        }
        return keyChain;
    };
    /**
     * Turns regex strings into {@link RegExp} objects.
     */
    ConfigManager._unstringifyConfig = function (config) {
        for (var characteristicUuid in config.known_data) {
            var characteristic = config.known_data[characteristicUuid];
            for (var _i = 0, characteristic_1 = characteristic; _i < characteristic_1.length; _i++) {
                var data = characteristic_1[_i];
                var flagsStart = data.match.lastIndexOf('/');
                var regex = data.match.substring(1, flagsStart);
                var flags = data.match.substr(flagsStart + 1);
                data.match = new RegExp(regex, flags);
            }
        }
        return config;
    };
    ;
    /**
     * @return Parent object of the trailing key and the trailing key.
     */
    ConfigManager._get = function (keyChain) {
        var parent = exports.config;
        var obj = exports.config;
        for (var _i = 0, keyChain_1 = keyChain; _i < keyChain_1.length; _i++) {
            var key = keyChain_1[_i];
            if (key in obj) {
                parent = obj;
                obj = obj[key];
            }
            else {
                return undefined;
            }
        }
        return {
            parent: parent,
            key: keyChain[keyChain.length - 1]
        };
    };
    /**
     * @param value Will be {@link JSON.parse}d
     * @return The old value of the config entry or undefined if @keyChain is
     *         invalid.
     */
    ConfigManager._set = function (keyChain, value) {
        var entry = this._get(keyChain);
        if (entry === undefined) {
            return undefined;
        }
        else {
            var jsValue = void 0;
            try {
                jsValue = JSON.parse(value);
            }
            catch (_a) {
                console.warn("invalid json value: " + value);
                return undefined;
            }
            // This is a very basic check, it's not safe.
            if (typeof (entry.parent[entry.key]) === typeof (jsValue)) {
                var oldValue = entry.parent[entry.key];
                entry.parent[entry.key] = jsValue;
                return oldValue;
            }
            else {
                console.warn("invalid value type " + typeof (value) + ", expected " + typeof (entry.parent[entry.key]));
                return undefined;
            }
        }
    };
    /**
     *
     */
    ConfigManager._getListeners = function (keyChain) {
        return this._listeners.find(function (listener) { return listener.keyChain.every(function (el, index) { return (keyChain[index] === el); }); });
    };
    /**
     * Invokes listeners on @keyChainListener but not on sub key chains.
     * @param keyChainListener The key chain the listener is attached to.
     * @param keyChainTarget The key chain that was updated.
     */
    ConfigManager._invokeListenersImpl = function (keyChainListener, keyChainTarget, oldValue, newValue) {
        for (var _i = 0, _a = this._listeners; _i < _a.length; _i++) {
            var listener = _a[_i];
            if (listener.keyChain.length !== keyChainListener.length) {
                continue;
            }
            if (this._getListeners(keyChainListener) !== undefined) {
                for (var _b = 0, _c = listener.listeners; _b < _c.length; _b++) {
                    var callback = _c[_b];
                    callback(keyChainTarget, oldValue, newValue);
                }
                break;
            }
        }
    };
    /**
     * Invokes listeners on @keyChain and all sub-keychains.
     */
    ConfigManager._invokeListeners = function (keyChain, oldValue, newValue) {
        if (keyChain.length > 0) {
            var subChain = [];
            for (var _i = 0, keyChain_2 = keyChain; _i < keyChain_2.length; _i++) {
                var key = keyChain_2[_i];
                subChain.push(key);
                this._invokeListenersImpl(subChain, keyChain, oldValue, newValue);
            }
        }
    };
    /**
     *
     */
    ConfigManager.set = function (keyChainString, value) {
        var keyChain;
        try {
            keyChain = this._keyChainFromString(keyChainString);
        }
        catch (ex) {
            // {@link #set} is called without knowing if the user's intention is to
            // access the config.
            // console.log(ex);
            return false;
        }
        var oldValue = this._set(keyChain, value);
        if (oldValue !== undefined) {
            this._invokeListeners(keyChain, oldValue, value);
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * Attaches a config change listener to a value or an object.
     */
    ConfigManager.attachChangeListener = function (keyChainString, listener) {
        return __awaiter(this, void 0, void 0, function () {
            var keyChain, listeners;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._configLoaded()];
                    case 1:
                        _a.sent();
                        keyChain = this._keyChainFromString(keyChainString);
                        if (this._get(keyChain) === undefined) {
                            console.error("tried to attach a listener to " + keyChain + ", which doesn't exist");
                            return [2 /*return*/];
                        }
                        listeners = this._getListeners(keyChain);
                        if (listeners === undefined) {
                            this._listeners.push({
                                keyChain: keyChain,
                                listeners: [listener],
                            });
                        }
                        else {
                            listeners.listeners.push(listener);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     *
     */
    ConfigManager.setConfig = function (receivedConfig) {
        Object.assign(exports.config, this._unstringifyConfig(receivedConfig));
        this._loadedCallbacks.forEach(function (el) { return el(); });
    };
    /**
     * Config change listeners.
     */
    ConfigManager._listeners = [];
    /**
     * All resolve calls waiting for the config to be loaded.
     */
    ConfigManager._loadedCallbacks = [];
    return ConfigManager;
}());
exports.ConfigManager = ConfigManager;
//# sourceMappingURL=config.js.map