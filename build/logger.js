"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Logger = /** @class */ (function () {
    function Logger() {
    }
    Logger._getHumanTime = function () {
        var makeString = function (i) {
            var str = i.toString();
            if (i < 10) {
                str = '0' + str;
            }
            return str;
        };
        var date = new Date();
        return date.getFullYear() + "-" + makeString(date.getMonth() + 1) + "-" + makeString(date.getDay()) +
            (" " + makeString(date.getHours()) + ":" + makeString(date.getMinutes()) + ":" + makeString(date.getSeconds()));
    };
    /**
     * @param {string} functionName
     * @param {Object} args
     */
    Logger.logFunctionCall = function (functionName, args) {
        console.log("[" + this._getHumanTime() + "] " + functionName + "(" + (args ? '' : ')'));
        if (args) {
            for (var _i = 0, _a = Object.keys(args); _i < _a.length; _i++) {
                var key = _a[_i];
                console.log("  " + key + ": " + args[key]);
            }
            console.log(')');
        }
    };
    return Logger;
}());
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map