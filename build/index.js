"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var polyfill_js_1 = require("./polyfill/polyfill.js");
polyfill_js_1.polyfill();
var callbackManager_js_1 = require("./callbackManager.js");
var config_js_1 = require("./config.js");
var commands_js_1 = require("./commands.js");
var Main = /** @class */ (function () {
    /**
     *
     */
    function Main() {
        // android.bluetooth.BluetoothGattService$1
        // android.bluetooth.IBluetoothGattServerCallback$Stub$Proxy
        // android.bluetooth.BluetoothGattIncludedService$1
        // android.bluetooth.IBluetoothGatt$Stub
        // android.bluetooth.BluetoothGattDescriptor
        // android.bluetooth.IBluetoothGattServerCallback
        // android.bluetooth.IBluetoothGatt
        // android.bluetooth.BluetoothGattService
        // android.bluetooth.IBluetoothGattServerCallback$Stub
        // android.bluetooth.BluetoothGattDescriptor$1
        // android.bluetooth.BluetoothGattCharacteristic$1
        // android.bluetooth.BluetoothGattCharacteristic
        // android.bluetooth.IBluetoothGatt$Stub$Proxy
        // Java.enumerateLoadedClasses({
        //   onMatch: (className) =>
        //   {
        //     if (className.includes('BluetoothGatt'))
        //     {
        //       console.log(className);
        //     }
        //   },
        //   onComplete: () => console.log('done')
        // });
        // return;
        callbackManager_js_1.callbacks.installCallbacks();
        this._cli().catch(function (reason) {
            console.error(reason.stack);
        });
    }
    /**
     *
     */
    Main.prototype._asyncRecv = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        recv(resolve);
                    })];
            });
        });
    };
    /**
     * Starts CLI mode.
     */
    Main.prototype._cli = function () {
        return __awaiter(this, void 0, void 0, function () {
            var commandLine, split, isQuoted, part, _i, commandLine_1, ch;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 4];
                        commands_js_1.Commands.exposeAliases();
                        send('frida-android-ble');
                        return [4 /*yield*/, this._asyncRecv()];
                    case 1:
                        commandLine = _a.sent();
                        if (!(commandLine.length > 0)) return [3 /*break*/, 3];
                        split = [];
                        isQuoted = false;
                        part = '';
                        for (_i = 0, commandLine_1 = commandLine; _i < commandLine_1.length; _i++) {
                            ch = commandLine_1[_i];
                            switch (ch) {
                                case '"':
                                    isQuoted = !isQuoted;
                                    break;
                                case ' ':
                                    if (!isQuoted) {
                                        split.push(part);
                                        part = '';
                                        break;
                                    }
                                default:
                                    part += ch;
                                    break;
                            }
                        }
                        if (part.length > 0) {
                            split.push(part);
                        }
                        return [4 /*yield*/, commands_js_1.Commands.onCommand(split[0], split.slice(1))];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 0];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return Main;
}());
function main() {
    recv('config', function (cfg) {
        config_js_1.ConfigManager.setConfig(cfg.payload);
        setTimeout(function () { return Java.perform(function () { return new Main(); }); }, 0);
    });
}
main();
//# sourceMappingURL=index.js.map