"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var history_js_1 = require("./history.js");
var logger_js_1 = require("./logger.js");
var human_js_1 = require("./human.js");
var config_js_1 = require("./config.js");
function toJSArray(javaArray) {
    var result = [];
    for (var _i = 0, javaArray_1 = javaArray; _i < javaArray_1.length; _i++) {
        var el = javaArray_1[_i];
        result.push(el);
    }
    return result;
}
var BluetoothGatt = /** @class */ (function () {
    /**
     *
     */
    function BluetoothGatt() {
        this._className = 'BluetoothGatt';
    }
    /**
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     */
    BluetoothGatt.prototype.writeCharacteristic = function (context, characteristic) {
        exports.callbacks.enabled = false;
        var characteristicValue = characteristic.getValue();
        exports.callbacks.enabled = true;
        var humanDevice = new human_js_1.Device(context.this.getDevice());
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic.getService());
        var humanData = new human_js_1.Data(characteristic, characteristicValue);
        var historyId = history_js_1.historyManager.pushBytes(history_js_1.HistoryType.WRITE, toJSArray(characteristicValue));
        logger_js_1.Logger.logFunctionCall(this._className + ".writeCharacteristic", {
            'device': humanDevice.getDisplayName(),
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': "[" + history_js_1.historyManager.getHistoryName(history_js_1.HistoryType.WRITE) + " " + historyId + "] " + humanData.getDisplayName(),
        });
    };
    /**
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     */
    BluetoothGatt.prototype.readCharacteristic = function (context, characteristic) {
        exports.callbacks.enabled = false;
        var characteristicValue = characteristic.getValue();
        exports.callbacks.enabled = true;
        var humanDevice = new human_js_1.Device(context.this.getDevice());
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic.getService());
        var humanData = new human_js_1.Data(characteristic, characteristicValue);
        logger_js_1.Logger.logFunctionCall(this._className + ".readCharacteristic", {
            'device': humanDevice.getDisplayName(),
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': humanData.getDisplayName(),
        });
    };
    /**
     * @param {android.bluetooth.BluetoothGattDescriptor} descriptor
     */
    BluetoothGatt.prototype.writeDescriptor = function (context, descriptor) {
        var device = context.this.getDevice();
        var characteristic = descriptor.getCharacteristic();
        var humanDevice = new human_js_1.Device(device);
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic ? characteristic.getService() : null);
        var humanData = new human_js_1.Data(characteristic, descriptor.getValue());
        var historyId = history_js_1.historyManager.pushBytes(history_js_1.HistoryType.WRITE, toJSArray(descriptor.getValue()));
        logger_js_1.Logger.logFunctionCall(this._className + ".writeDescriptor", {
            'device': humanDevice.getDisplayName(),
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': "[" + history_js_1.historyManager.getHistoryName(history_js_1.HistoryType.WRITE) + " " + historyId + "] " + humanData.getDisplayName(),
        });
    };
    return BluetoothGatt;
}());
var BluetoothGattCallback = /** @class */ (function () {
    function BluetoothGattCallback() {
        this._className = 'BluetoothGattCallback';
    }
    /**
     * @param {android.bluetooth.BluetoothGatt} gatt
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     */
    BluetoothGattCallback.prototype.onCharacteristicChanged = function (context, gatt, characteristic) {
        exports.callbacks.enabled = false;
        var value = characteristic.getValue();
        exports.callbacks.enabled = true;
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic.getService());
        var humanData = new human_js_1.Data(characteristic, value);
        var historyId = history_js_1.historyManager.pushBytes(history_js_1.HistoryType.READ, toJSArray(value));
        logger_js_1.Logger.logFunctionCall(this._className + ".onCharacteristicChanged", {
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': "[" + history_js_1.historyManager.getHistoryName(history_js_1.HistoryType.READ) + " " + historyId + "] " + humanData.getDisplayName(),
        });
    };
    return BluetoothGattCallback;
}());
var BluetoothGattCharacteristic = /** @class */ (function () {
    function BluetoothGattCharacteristic() {
        this._className = 'BluetoothGattCharacteristic';
    }
    /**
     *
     */
    BluetoothGattCharacteristic.prototype.getValue = function (context) {
        var _this = this;
        var humanCharacteristic = new human_js_1.Characteristic(context.this);
        var humanService = new human_js_1.Service(context.this.getService());
        var humanData = new human_js_1.Data(context.this, context.retval);
        var historyId = history_js_1.historyManager.pushBytes(history_js_1.HistoryType.READ, toJSArray(context.retval));
        if (config_js_1.config.debug_on_characteristic_changed) {
            Java.perform(function () {
                // I'm sure there's a nicer way, but this is for debugging only and I
                // don't like Java.
                var trace = Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new());
                var lines = trace.split('\n');
                if (lines.length >= 3) {
                    if (lines[2].includes('.onCharacteristicChanged(')) {
                        console.log(_this._className + ".getValue called", lines[2].trim());
                    }
                }
            });
        }
        logger_js_1.Logger.logFunctionCall(this._className + ".getValue", {
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': "[" + history_js_1.historyManager.getHistoryName(history_js_1.HistoryType.READ) + " " + historyId + "] " + humanData.getDisplayName(),
        });
    };
    return BluetoothGattCharacteristic;
}());
var Callbacks = /** @class */ (function () {
    /**
     *
     */
    function Callbacks() {
        /**
         * Enable hooks. Used to prevent recursive and internal calls from within
         * hooks.
         */
        this.enabled = true;
        this['android.bluetooth.BluetoothGatt'] = new BluetoothGatt();
        this['android.bluetooth.BluetoothGattCallback'] = new BluetoothGattCallback();
        this['android.bluetooth.BluetoothGattCharacteristic'] = new BluetoothGattCharacteristic();
    }
    /**
     * Used for functions without a custom callback implementation.
     */
    Callbacks.prototype.genericCallback = function (name, context) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var javaObject = Java.use('java.lang.Object');
        var logArgs = {};
        var i = 0;
        for (var _a = 0, args_1 = args; _a < args_1.length; _a++) {
            var arg = args_1[_a];
            logArgs["a" + i++] = Java.cast(arg, javaObject).toString();
        }
        logger_js_1.Logger.logFunctionCall(name, logArgs);
    };
    return Callbacks;
}());
exports.callbacks = new Callbacks();
//# sourceMappingURL=callbacks.js.map