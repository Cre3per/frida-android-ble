"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var history_1 = require("./history");
var human_1 = require("./human");
var formatter_1 = require("./formatter");
var color_1 = require("./color");
var diff_1 = require("./diff");
var writer_1 = require("./writer");
var config_1 = require("./config");
var Command = /** @class */ (function () {
    function Command(data) {
        /**
         * Usage syntax.
         */
        this.usage = '';
        /**
         *
         */
        this.description = '';
        if (!data.alias || !data.handler) {
            console.error('created command without alias or handler.');
        }
        Object.assign(this, data);
    }
    return Command;
}());
var Commands = /** @class */ (function () {
    function Commands() {
    }
    /**
     *
     */
    Commands._help = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _i, _a, command;
            return __generator(this, function (_b) {
                for (_i = 0, _a = this._commands; _i < _a.length; _i++) {
                    command = _a[_i];
                    console.log(command.usage + " - " + command.description);
                }
                console.log('or a config key chain and value, eg. "callbacks[android.bluetooth.BluetoothGatt].getService true"');
                return [2 /*return*/, true];
            });
        });
    };
    /**
     *
     */
    Commands._printByteDiff = function (a, b, diff) {
        var minLength = Math.min(a.length, b.length);
        var generateDiffLine = function (source) {
            var line = '';
            for (var i = 0; i < diff.length; ++i) {
                if (i < source.length) {
                    var byte = formatter_1.Formatter.byteToString(source[i]);
                    if (diff[i]) {
                        line += byte;
                    }
                    else {
                        if (i >= minLength) {
                            // Won't be colored if config.color is disabled, but different
                            // lengths don't need highlighting.
                            line += color_1.Color.PURPLE(byte);
                        }
                        else {
                            if (config_1.config.color) {
                                line += color_1.Color.RED(byte);
                            }
                            else {
                                line += "(" + byte + ")";
                            }
                        }
                    }
                    if (i < (source.length - 1)) {
                        line += ' ';
                    }
                }
            }
            return line;
        };
        console.log(generateDiffLine(a));
        console.log(generateDiffLine(b));
    };
    /**
     *
     */
    Commands._diff = function (args) {
        return __awaiter(this, void 0, void 0, function () {
            var historyType, a, b;
            return __generator(this, function (_a) {
                if (args.length === 3) {
                    historyType = history_1.historyManager.getHistoryByName(args[0]);
                    if (historyType !== undefined) {
                        a = history_1.historyManager.getBytesAt(historyType, parseInt(args[1]));
                        b = history_1.historyManager.getBytesAt(historyType, parseInt(args[2]));
                        if (a && b) {
                            this._printByteDiff(a, b, diff_1.Diff.bytes(a, b));
                            return [2 /*return*/, true];
                        }
                    }
                }
                return [2 /*return*/, false];
            });
        });
    };
    /**
     *
     */
    Commands._history = function (args) {
        return __awaiter(this, void 0, void 0, function () {
            var historyType, bytes, _i, bytes_1, entry, humanData;
            return __generator(this, function (_a) {
                if (args.length === 1) {
                    historyType = history_1.historyManager.getHistoryByName(args[0]);
                    if (historyType !== undefined) {
                        bytes = history_1.historyManager.getBytes(historyType);
                        console.log(bytes.length + " entries in history " + args[0]);
                        for (_i = 0, bytes_1 = bytes; _i < bytes_1.length; _i++) {
                            entry = bytes_1[_i];
                            humanData = new human_1.Data(null, entry.data);
                            console.log(entry.index + "] " + humanData.getDisplayName());
                        }
                        return [2 /*return*/, true];
                    }
                }
                return [2 /*return*/, false];
            });
        });
    };
    /**
     *
     */
    Commands._listCharacteristics = function (args) {
        return __awaiter(this, void 0, void 0, function () {
            var gatts;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (args.length !== 0) {
                            return [2 /*return*/, false];
                        }
                        return [4 /*yield*/, this._writer.update()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this._writer.getGatts()];
                    case 2:
                        gatts = _a.sent();
                        Java.perform(function () {
                            for (var _i = 0, gatts_1 = gatts; _i < gatts_1.length; _i++) {
                                var gatt = gatts_1[_i];
                                var humanDevice = new human_1.Device(gatt.handle.getDevice());
                                console.log(humanDevice.getDisplayName());
                                for (var _a = 0, _b = gatt.services; _a < _b.length; _a++) {
                                    var service = _b[_a];
                                    var humanService = new human_1.Service(service.handle);
                                    console.log('  ' + humanService.getDisplayName());
                                    for (var _c = 0, _d = service.characteristics; _c < _d.length; _c++) {
                                        var characteristic = _d[_c];
                                        var humanCharacteristic = new human_1.Characteristic(characteristic.handle);
                                        console.log('  ' + '  ' + characteristic.id + '] ' + humanCharacteristic.getDisplayName());
                                    }
                                }
                            }
                        });
                        return [2 /*return*/, true];
                }
            });
        });
    };
    /**
     *
     */
    Commands._highlightRegExpMatches = function (str, matches) {
        var result = '';
        var nextStart = 0;
        for (var _i = 0, matches_1 = matches; _i < matches_1.length; _i++) {
            var match = matches_1[_i];
            result += str.substring(nextStart, match.index);
            if (config_1.config.color) {
                result += color_1.Color.yellow(match[0]);
            }
            else {
                result += "(" + match[0] + ")";
            }
            nextStart = (match.index + match[0].length);
        }
        result += str.substr(nextStart);
        return result;
    };
    /**
     *
     */
    Commands._searchHistory = function (args) {
        return __awaiter(this, void 0, void 0, function () {
            var historyName, historyType, flagsStart, regex, flags, pattern, history, _i, history_2, entry, strData, matches;
            return __generator(this, function (_a) {
                if (args.length === 2) {
                    historyName = args[0];
                    historyType = history_1.historyManager.getHistoryByName(historyName);
                    if (historyType !== undefined) {
                        if (args[1][0] !== '/') {
                            args[1] = "/" + args[1] + "/g";
                        }
                        flagsStart = args[1].lastIndexOf('/');
                        regex = args[1].substring(1, flagsStart);
                        flags = args[1].substr(flagsStart + 1);
                        if (!flags.includes('g')) {
                            flags += 'g';
                        }
                        pattern = void 0;
                        try {
                            pattern = new RegExp(regex, flags);
                        }
                        catch (_b) {
                            return [2 /*return*/, false];
                        }
                        history = history_1.historyManager.getBytes(historyType);
                        for (_i = 0, history_2 = history; _i < history_2.length; _i++) {
                            entry = history_2[_i];
                            strData = formatter_1.Formatter.byteArrayToString(entry.data);
                            matches = strData.matchAll(pattern);
                            if (matches.length > 0) {
                                // We can't resolve to a custom name, because we'd need the
                                // characteristic's handle, which we didn't store.
                                console.log("[" + historyName + " " + entry.index + "] " + this._highlightRegExpMatches(strData, matches));
                            }
                        }
                        return [2 /*return*/, true];
                    }
                }
                return [2 /*return*/, false];
            });
        });
    };
    /**
     *
     */
    Commands._writeCharacteristic = function (args) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (args.length === 2) {
                    return [2 /*return*/, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                            var bytes, characteristicId;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        bytes = formatter_1.Formatter.stringToByteArray(args[1]);
                                        if (!bytes) return [3 /*break*/, 2];
                                        characteristicId = void 0;
                                        if (!(formatter_1.Formatter.isDecimalNumber(args[0]) && (characteristicId = parseInt(args[0]), !isNaN(characteristicId)))) {
                                            characteristicId = args[0];
                                        }
                                        return [4 /*yield*/, this._writer.writeCharacteristic(characteristicId, bytes)];
                                    case 1:
                                        if (!(_a.sent())) {
                                            console.log('write failed');
                                        }
                                        resolve(true);
                                        return [3 /*break*/, 3];
                                    case 2:
                                        resolve(false);
                                        _a.label = 3;
                                    case 3: return [2 /*return*/];
                                }
                            });
                        }); })];
                }
                return [2 /*return*/, false];
            });
        });
    };
    /**
     *
     */
    Commands.onCommand = function (command, args) {
        return __awaiter(this, void 0, void 0, function () {
            var cmd;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        cmd = this._commands.find(function (el) { return el.alias.includes(command); });
                        if (!cmd) return [3 /*break*/, 2];
                        return [4 /*yield*/, cmd.handler.call(this, args)];
                    case 1:
                        if (!(_a.sent())) {
                            console.log(cmd.usage);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        if ((args.length === 1) && (config_1.ConfigManager.set(command, args[0]))) {
                            // all good
                        }
                        else {
                            console.log(command + " was not recognized as a command or config entry. see 'help' for a list of commands.");
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Sends all aliases to the host.
     */
    Commands.exposeAliases = function () {
        var aliases = '';
        for (var _i = 0, _a = this._commands; _i < _a.length; _i++) {
            var command = _a[_i];
            for (var _b = 0, _c = command.alias; _b < _c.length; _b++) {
                var alias = _c[_b];
                aliases += alias;
                aliases += ',';
            }
        }
        // Remove trailing comma.
        aliases = aliases.substr(0, aliases.length - 1);
        send("aliases:" + aliases);
    };
    /**
     *
     */
    Commands._writer = new writer_1.Writer();
    /**
     *
     */
    Commands._commands = [
        new Command({
            alias: ['diff', 'd'],
            usage: 'diff [type] [id] [id]',
            description: 'diffs the specified entries of a history',
            handler: Commands._diff,
        }),
        new Command({
            alias: ['help', '?', 'commands'],
            usage: 'help',
            description: 'shows available commands',
            handler: Commands._help,
        }),
        new Command({
            alias: ['history', 'h'],
            usage: 'history [type]',
            description: 'displays the specified history. entries can be used with \'diff\'',
            handler: Commands._history,
        }),
        new Command({
            alias: ['listCharacteristics', 'lc'],
            usage: 'listCharacteristics',
            description: 'lists all active characteristics',
            handler: Commands._listCharacteristics,
        }),
        new Command({
            alias: ['searchHistory', 'search', 'find', 's', 'f'],
            usage: 'searchHistory [history] [regexp]',
            description: 'lists all entries in the specified history matching a regular expression',
            handler: Commands._searchHistory,
        }),
        new Command({
            alias: ['writeCharacteristic', 'wc', 'w'],
            usage: 'writeCharacteristic [id] [data]',
            description: 'writes data the the specified characteristic',
            handler: Commands._writeCharacteristic,
        }),
    ];
    return Commands;
}());
exports.Commands = Commands;
//# sourceMappingURL=commands.js.map