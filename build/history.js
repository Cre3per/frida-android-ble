"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("./config");
var HistoryType;
(function (HistoryType) {
    HistoryType[HistoryType["READ"] = 0] = "READ";
    HistoryType[HistoryType["WRITE"] = 1] = "WRITE";
    HistoryType[HistoryType["COUNT"] = 2] = "COUNT";
})(HistoryType = exports.HistoryType || (exports.HistoryType = {}));
var HistoryEntry = /** @class */ (function () {
    function HistoryEntry(index, data) {
        this.index = index;
        this.data = data;
    }
    return HistoryEntry;
}());
exports.HistoryEntry = HistoryEntry;
var HistoryManager = /** @class */ (function () {
    function HistoryManager() {
        this._histories = [];
        var types = [
            {
                type: HistoryType.READ,
                name: 'read',
            },
            {
                type: HistoryType.WRITE,
                name: 'write',
            },
        ];
        for (var _i = 0, types_1 = types; _i < types_1.length; _i++) {
            var history = types_1[_i];
            this._histories.push({
                type: history.type,
                name: history.name,
                history: [],
                nextIndex: 0,
            });
        }
        if (this._histories.length !== HistoryType.COUNT) {
            console.error("this._histories.length (" + this._histories.length + ") !== HistoryType.length (" + Object.keys(HistoryType).length + ")");
        }
    }
    /**
     * @return History of type @type
     */
    HistoryManager.prototype.getHistory = function (type) {
        return this._histories.find(function (el) { return (el.type === type); });
    };
    /**
     *
     */
    HistoryManager.prototype.getHistoryByName = function (name) {
        var found = this._histories.find(function (el) { return (el.name === name); });
        if (found) {
            return found.type;
        }
        else {
            return undefined;
        }
    };
    /**
     *
     */
    HistoryManager.prototype.getHistoryName = function (history) {
        return this.getHistory(history).name;
    };
    /**
     * @return Index of the pushed entry.
     */
    HistoryManager.prototype.pushBytes = function (historyType, data) {
        if (config_1.config.history_size > 0) {
            var history = this.getHistory(historyType);
            if (history.history.length >= config_1.config.history_size) {
                history.history.splice(0, (config_1.config.history_size - history.history.length) + 1);
            }
            return (history.history.push({
                index: history.nextIndex++,
                data: data
            }) - 1);
        }
        return 0;
    };
    /**
     * @return Byte history.
     */
    HistoryManager.prototype.getBytes = function (historyType) {
        return this.getHistory(historyType).history;
    };
    /**
     * @return Byte diff entry at @index.
     */
    HistoryManager.prototype.getBytesAt = function (historyType, index) {
        var history = this.getHistory(historyType).history;
        for (var _i = 0, history_1 = history; _i < history_1.length; _i++) {
            var el = history_1[_i];
            if (el.index === index) {
                return el.data;
            }
        }
        return undefined;
    };
    return HistoryManager;
}());
exports.historyManager = new HistoryManager();
//# sourceMappingURL=history.js.map