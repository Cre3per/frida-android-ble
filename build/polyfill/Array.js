"use strict";
Array.prototype.fill = function (value, start, end) {
    if (start === undefined) {
        start = 0;
    }
    if (end === undefined) {
        end = this.length;
    }
    for (var i = start; i < end; ++i) {
        this[i] = value;
    }
    return this;
};
//# sourceMappingURL=Array.js.map