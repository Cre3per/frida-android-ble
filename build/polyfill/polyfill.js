"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.polyfill = function () {
    Array.prototype.find = function (predicate, thisArg) {
        for (var i = 0; i < this.length; ++i) {
            if (predicate.call(thisArg, this[i], i, this)) {
                return this[i];
            }
        }
        return undefined;
    };
    Array.prototype.fill = function (value, start, end) {
        if (start === undefined) {
            start = 0;
        }
        if (end === undefined) {
            end = this.length;
        }
        for (var i = start; i < end; ++i) {
            this[i] = value;
        }
        return this;
    };
    Array.prototype.includes = function (valueToFind, fromIndex) {
        return (this.indexOf(valueToFind) !== -1);
    };
    String.prototype.matchAll = function (regexp) {
        var result = [];
        var originalLastIndex = regexp.lastIndex;
        regexp.lastIndex = 0;
        var str = this.valueOf();
        var match;
        while (match = regexp.exec(str)) {
            result.push(match);
        }
        regexp.lastIndex = originalLastIndex;
        return result;
    };
};
//# sourceMappingURL=polyfill.js.map