"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var config_js_1 = require("./config.js");
var callbacks_js_1 = require("./callbacks.js");
var Main = /** @class */ (function () {
    /**
     *
     */
    function Main() {
        this._installCallbacks();
    }
    /**
     *
     */
    Main.prototype._installCallback = function (javaClass, functionName, callback) {
        javaClass[functionName].implementation = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            callback.apply(void 0, [this].concat(args));
            return this[functionName].apply(this, args);
        };
    };
    /**
     * Installs Java hooks.
     */
    Main.prototype._installCallbacks = function () {
        var callbacks = new callbacks_js_1.Callbacks();
        var _this = this;
        var javaBluetoothGatt = Java.use('android.bluetooth.BluetoothGatt');
        var javaBluetoothGattCallback = Java.use('android.bluetooth.BluetoothGattCallback');
        var javaBluetoothDevice = Java.use('android.bluetooth.BluetoothDevice');
        var _loop_1 = function (functionName) {
            if (config_js_1.config.callbacks.BluetoothGatt[functionName]) {
                this_1._installCallback(javaBluetoothGatt, functionName, function () {
                    var _a;
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    return (_a = callbacks.BluetoothGatt)[functionName].apply(_a, args);
                });
            }
        };
        var this_1 = this;
        for (var _i = 0, _a = Object.keys(config_js_1.config.callbacks.BluetoothGatt); _i < _a.length; _i++) {
            var functionName = _a[_i];
            _loop_1(functionName);
        }
        // ---
        javaBluetoothGattCallback.onCharacteristicChanged.implementation = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            console.log('onCharacteristicChanged');
            return this.onCharacteristicChanged.apply(this, args);
        };
        javaBluetoothGattCallback.onCharacteristicRead.implementation = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            console.log('onCharacteristicRead');
            return this.onCharacteristicRead.apply(this, args);
        };
        javaBluetoothGatt.readCharacteristic.implementation = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            console.log('readc');
            return this.readCharacteristic.apply(this, args);
        };
        javaBluetoothGatt.readDescriptor.implementation = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            console.log('readDescriptor');
            return this.readDescriptor.apply(this, args);
        };
        javaBluetoothGattCallback.onDescriptorRead.implementation = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            console.log('onDescriptorRead');
            return this.onDescriptorRead.apply(this, args);
        };
    };
    return Main;
}());
setTimeout(function () { return Java.perform(function () { return new Main(); }); }, 0);
//# sourceMappingURL=index.js.map