"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Logger = /** @class */ (function () {
    function Logger() {
    }
    /**
     * @param {string} functionName
     * @param {Object} args
     */
    Logger.logFunctionCall = function (functionName, args) {
        console.log("[" + (new Date()).toISOString() + "] " + functionName + "(" + (args ? '' : ')'));
        if (args) {
            for (var _i = 0, _a = Object.keys(args); _i < _a.length; _i++) {
                var key = _a[_i];
                console.log("  " + key + ": " + args[key]);
            }
            console.log(')');
        }
    };
    return Logger;
}());
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map