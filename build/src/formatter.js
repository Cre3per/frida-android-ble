"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Formatter = /** @class */ (function () {
    function Formatter() {
    }
    /**
     * Converts a byte array to human readable format (lower case hex).
     * @param {number[]} arr
     * @return {string}
     */
    Formatter.byteArrayToString = function (arr) {
        var str = '';
        for (var i = 0; i < arr.length; ++i) {
            var el = arr[i];
            if (el < 0) {
                el += 256;
            }
            if (el < 0x10) {
                str += '0';
            }
            str += el.toString(16);
            if (i !== (arr.length - 1)) {
                str += ' ';
            }
        }
        return str;
    };
    return Formatter;
}());
exports.Formatter = Formatter;
//# sourceMappingURL=formatter.js.map