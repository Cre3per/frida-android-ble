"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var config_js_1 = require("./config.js");
var logger_js_1 = require("./logger.js");
var human_js_1 = require("./human.js");
var IgnorableCallbacks = /** @class */ (function () {
    /**
     *
     */
    function IgnorableCallbacks() {
        /**
         * Maps function names to the hash of their arguments.
         * @type {[key: string]: { time: number, args: string }
         */
        this._previousCalls = {};
    }
    /**
     * Saves the function call.
     * @return {boolean} True if the call should be ignored.
     */
    IgnorableCallbacks.prototype._shouldIgnore = function (functionName) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return false;
        var previousCall = this._previousCalls[functionName];
        var time = (new Date()).getTime();
        var hashArgs = '';
        // TODO: find a reliable argument hashing method.
        this._previousCalls[functionName] = {
            time: time,
            args: hashArgs,
        };
        if (previousCall && ((time - previousCall.time) < config_js_1.config.skip_repeated_callbacks)) {
            return (previousCall.args === hashArgs);
        }
        else {
            return false;
        }
    };
    return IgnorableCallbacks;
}());
var BluetoothGatt = /** @class */ (function (_super) {
    __extends(BluetoothGatt, _super);
    /**
     *
     */
    function BluetoothGatt() {
        var _this_1 = _super.call(this) || this;
        _this_1._className = 'android.bluetooth.BluetoothGatt';
        return _this_1;
    }
    /**
     * @param {android.bluetooth.BluetoothGatt} _this
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     */
    BluetoothGatt.prototype.writeCharacteristic = function (_this, characteristic) {
        if (this._shouldIgnore(this._className + ".writeCharacteristic", _this, characteristic)) {
            return;
        }
        var humanDevice = new human_js_1.Device(_this.getDevice());
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic.getService());
        var humanData = new human_js_1.Data(characteristic, characteristic.getValue());
        logger_js_1.Logger.logFunctionCall('writeCharacteristic', {
            'device': humanDevice.getDisplayName(),
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': humanData.getDisplayName(),
        });
    };
    /**
     * @param {android.bluetooth.BluetoothGatt} _this
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     */
    BluetoothGatt.prototype.readCharacteristic = function (_this, characteristic) {
        var humanDevice = new human_js_1.Device(_this.getDevice());
        var humanCharacteristic = new human_js_1.Characteristic(characteristic);
        var humanService = new human_js_1.Service(characteristic.getService());
        var humanData = new human_js_1.Data(characteristic, characteristic.getValue());
        logger_js_1.Logger.logFunctionCall('readCharacteristic', {
            'device': humanDevice.getDisplayName(),
            'service': humanService.getDisplayName(),
            'characteristic': humanCharacteristic.getDisplayName(),
            'data': humanData.getDisplayName(),
        });
    };
    /**
     * @param {android.bluetooth.BluetoothGatt} _this
     * @param {android.bluetooth.BluetoothGattDescriptor} descriptor
     */
    BluetoothGatt.prototype.writeDescriptor = function (_this, descriptor) {
        var device = _this.getDevice();
        var characteristic = descriptor.getCharacteristic();
        var service = (characteristic ? characteristic.getService() : null);
        var humanData = new human_js_1.Data(characteristic, descriptor.getValue());
        logger_js_1.Logger.logFunctionCall('writeDescriptor', {
            'address': (device ? device.getAddress() : 'disconnected'),
            'service': (service ? service.getUuid().toString() : 'none'),
            'characteristic': (characteristic ? characteristic.getUuid().toString() : 'none'),
            'descriptor': descriptor.getUuid().toString(),
            'data': humanData.getDisplayName(),
        });
    };
    return BluetoothGatt;
}(IgnorableCallbacks));
var Callbacks = /** @class */ (function () {
    function Callbacks() {
        this.BluetoothGatt = new BluetoothGatt();
    }
    return Callbacks;
}());
exports.Callbacks = Callbacks;
//# sourceMappingURL=callbacks.js.map