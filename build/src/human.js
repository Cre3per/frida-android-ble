"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var config_js_1 = require("./config.js");
var formatter_js_1 = require("./formatter.js");
var IHumanizer = /** @class */ (function () {
    function IHumanizer() {
    }
    IHumanizer.prototype.getDisplayName = function () {
        console.log('IHumanizer.getDisplayName called');
        return 'IHumanizer.getDisplayName';
    };
    return IHumanizer;
}());
var Device = /** @class */ (function (_super) {
    __extends(Device, _super);
    /**
     * @param {android.bluetooth.BluetoothDevice} device
     */
    function Device(device) {
        var _this = _super.call(this) || this;
        _this._device = device;
        return _this;
    }
    /**
     * @return {string}
     */
    Device.prototype.getDisplayName = function () {
        if (this._device) {
            var address = this._device.getAddress();
            if (config_js_1.config.known_devices[address]) {
                return config_js_1.config.known_devices[address];
            }
            else {
                return this._device.getAddress() + " (" + this._device.getName() + ")";
            }
        }
        else {
            return 'none';
        }
    };
    return Device;
}(IHumanizer));
exports.Device = Device;
var Service = /** @class */ (function (_super) {
    __extends(Service, _super);
    /**
     * @param {android.bluetooth.BluetoothGattService} service
     */
    function Service(service) {
        var _this = _super.call(this) || this;
        _this._service = service;
        return _this;
    }
    /**
     * @return {string}
     */
    Service.prototype.getDisplayName = function () {
        if (this._service) {
            var uuid = this._service.getUuid();
            if (uuid) {
                var strUuid = uuid.toString();
                if (config_js_1.config.known_services[strUuid]) {
                    return config_js_1.config.known_services[strUuid];
                }
                else {
                    return strUuid;
                }
            }
            else {
                return 'unknown';
            }
        }
        else {
            return 'none';
        }
    };
    return Service;
}(IHumanizer));
exports.Service = Service;
var Characteristic = /** @class */ (function (_super) {
    __extends(Characteristic, _super);
    /**
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     */
    function Characteristic(characteristic) {
        var _this = _super.call(this) || this;
        _this._characteristic = characteristic;
        return _this;
    }
    /**
     * @return {string}
     */
    Characteristic.prototype.getDisplayName = function () {
        if (this._characteristic) {
            var uuid = this._characteristic.getUuid();
            if (uuid) {
                var strUuid = uuid.toString();
                if (config_js_1.config.known_characteristics[strUuid]) {
                    return config_js_1.config.known_characteristics[strUuid];
                }
                else {
                    return strUuid;
                }
            }
            else {
                return 'unknown';
            }
        }
        else {
            return 'none';
        }
    };
    return Characteristic;
}(IHumanizer));
exports.Characteristic = Characteristic;
var Data = /** @class */ (function (_super) {
    __extends(Data, _super);
    /**
     * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
     * @param {byte[]} data
     */
    function Data(characteristic, data) {
        var _this = _super.call(this) || this;
        _this._characteristic = characteristic;
        _this._data = data;
        return _this;
    }
    /**
     * @return {string}
     */
    Data.prototype.getDisplayName = function () {
        if (this._data) {
            var strData = formatter_js_1.Formatter.byteArrayToString(this._data);
            if (this._characteristic && this._characteristic.getUuid()) {
                var uuid = this._characteristic.getUuid().toString();
                var knownData = config_js_1.config.known_data[uuid];
                if (knownData) {
                    for (var _i = 0, _a = config_js_1.config.known_data[uuid]; _i < _a.length; _i++) {
                        var data = _a[_i];
                        if (strData.match(data.match)) {
                            return data.name + " (" + strData + ")";
                        }
                    }
                }
            }
            return strData;
        }
        else {
            return 'none';
        }
    };
    return Data;
}(IHumanizer));
exports.Data = Data;
//# sourceMappingURL=human.js.map