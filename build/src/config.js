"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = {
    // If an identical call occurs within this many milliseconds of the previous
    // call, don't log it. Set to 0 to ignore nothing.
    skip_repeated_callbacks: 500,
    callbacks: {
        'BluetoothGatt': {
            'writeCharacteristic': true,
            'writeDescriptor': true
        }
    },
    known_devices: {
        'C0:57:65:BC:FA:C7': 'Makibes HR3',
    },
    known_services: {
        '6e400001-b5a3-f393-e0a9-e50e24dcca9e': 'Nordic UART',
    },
    known_characteristics: {
        '6e400002-b5a3-f393-e0a9-e50e24dcca9e': 'RX'
    },
    known_data: {
        '6e400002-b5a3-f393-e0a9-e50e24dcca9e': [
            {
                match: /^ab 00 [0-f]{2} ff 23/,
                name: 'CMD_FACTORY_RESET'
            },
            {
                match: /^ab 00 [0-f]{2} ff 72/,
                name: 'CMD_SEND_NOTIFICATION'
            },
            {
                match: /^ab 00 [0-f]{2} ff 73/,
                name: 'CMD_SET_ALARM_REMINDER'
            },
            {
                match: /^ab 00 [0-f]{2} ff 74/,
                name: 'CMD_SET_PERSONAL_INFORMATION'
            },
            {
                match: /^ab 00 [0-f]{2} ff 75/,
                name: 'CMD_SET_SEDENTARY_REMINDER'
            },
            {
                match: /^ab 00 [0-f]{2} ff 76/,
                name: 'CMD_SET_QUITE_HOURS'
            },
            {
                match: /^ab 00 [0-f]{2} ff 77/,
                name: 'CMD_SET_HEADS_UP_SCREEN'
            },
            {
                match: /^ab 00 [0-f]{2} ff 79/,
                name: 'CMD_SET_PHOTOGRAPH_MODE'
            },
            {
                match: /^ab 00 [0-f]{2} ff 7a/,
                name: 'CMD_LOST_REMIDER'
            },
            {
                match: /^ab 00 [0-f]{2} ff 7c/,
                name: 'CMD_SET_TIMEMODE'
            },
            {
                match: /^ab 00 [0-f]{2} ff 93/,
                name: 'CMD_SET_DATE_TIME'
            },
            {
                match: /^ab 00 03 ff ff 80$/,
                name: 'CMD_REBOOT'
            },
        ]
    }
};
//# sourceMappingURL=config.js.map