"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Formatter = /** @class */ (function () {
    function Formatter() {
    }
    /**
     * Converts a single byte to a hex string.
     */
    Formatter.byteToString = function (byte) {
        if (byte < 0) {
            byte += 0x100;
        }
        var hex = byte.toString(16);
        if (byte < 0x10) {
            return "0" + hex;
        }
        else {
            return hex;
        }
    };
    /**
     * Converts a single byte from string to byte.
     * Requires lower case hex.
     * @return -127 to 128
     */
    Formatter.stringToByte = function (str) {
        var byte = parseInt(str, 16);
        if (isNaN(byte) || (byte < 0x00) || (byte > 0xff)) {
            return undefined;
        }
        else {
            if (byte > 0x7f) {
                byte -= 0x100;
            }
            return byte;
        }
    };
    /**
     * Converts a byte array to human readable format (lower case hex).
     */
    Formatter.byteArrayToString = function (arr) {
        var str = '';
        for (var i = 0; i < arr.length; ++i) {
            str += this.byteToString(arr[i]);
            if (i !== (arr.length - 1)) {
                str += ' ';
            }
        }
        return str;
    };
    /**
     * Converts a lower case, space separated, hex string to a byte array.
     * Byte values are -127 to 128. Hex values are 00 to ff.
     */
    Formatter.stringToByteArray = function (str) {
        var result = [];
        var split = str.split(' ');
        for (var _i = 0, split_1 = split; _i < split_1.length; _i++) {
            var part = split_1[_i];
            var byte = this.stringToByte(part);
            if (byte === undefined) {
                return undefined;
            }
            else {
                result.push(byte);
            }
        }
        return result;
    };
    /**
     *
     */
    Formatter.isDecimalDigit = function (char) {
        return '0123456789'.includes(char);
    };
    /**
     *
     */
    Formatter.isDecimalNumber = function (str) {
        return str.split('').every(this.isDecimalDigit);
    };
    /**
     * Converts a Java array to a js array.
     */
    Formatter.toJSArray = function (javaArray) {
        var result = [];
        for (var _i = 0, javaArray_1 = javaArray; _i < javaArray_1.length; _i++) {
            var el = javaArray_1[_i];
            result.push(el);
        }
        return result;
    };
    /**
     * Converts a Java string to a js string.
     */
    Formatter.toJSString = function (javaString) {
        if (typeof (javaString) === 'string') {
            return javaString;
        }
        var result = '';
        var length = javaString.length();
        for (var i = 0; i < length; ++i) {
            result += javaString.charAt(i);
        }
        return result;
    };
    return Formatter;
}());
exports.Formatter = Formatter;
//# sourceMappingURL=formatter.js.map