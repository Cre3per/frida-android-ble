import { polyfill } from './polyfill/polyfill.js';
polyfill();

import { callbacks } from './callbackManager.js';
import { ConfigManager } from './config.js';
import { Commands } from './commands.js';

class Main
{
  /**
   * 
   */
  private async _asyncRecv(): Promise<any>
  {
    return new Promise((resolve) =>
    {
      recv(resolve);
    });
  }

  /**
   * Starts CLI mode.
   */
  private async _cli()
  {
    while (true)
    {
      Commands.exposeAliases();
      send('frida-android-ble');
      let commandLine: string = await this._asyncRecv();

      if (commandLine.length > 0)
      {
        let split: string[] = [];
        let isQuoted = false;
        let part = '';

        for (let ch of commandLine)
        {
          switch (ch)
          {
            case '"':
              isQuoted = !isQuoted;
              break;
            case ' ':
              if (!isQuoted)
              {
                split.push(part);
                part = '';
                break;
              }
            default:
              part += ch;
              break;
          }
        }

        if (part.length > 0)
        {
          split.push(part);
        }

        await Commands.onCommand(split[0], split.slice(1));
      }
    }
  }

  /**
   * 
   */
  constructor()
  {
    // android.bluetooth.BluetoothGattService$1
    // android.bluetooth.IBluetoothGattServerCallback$Stub$Proxy
    // android.bluetooth.BluetoothGattIncludedService$1
    // android.bluetooth.IBluetoothGatt$Stub
    // android.bluetooth.BluetoothGattDescriptor
    // android.bluetooth.IBluetoothGattServerCallback
    // android.bluetooth.IBluetoothGatt
    // android.bluetooth.BluetoothGattService
    // android.bluetooth.IBluetoothGattServerCallback$Stub
    // android.bluetooth.BluetoothGattDescriptor$1
    // android.bluetooth.BluetoothGattCharacteristic$1
    // android.bluetooth.BluetoothGattCharacteristic
    // android.bluetooth.IBluetoothGatt$Stub$Proxy

    // Java.enumerateLoadedClasses({
    //   onMatch: (className) =>
    //   {
    //     if (className.includes('BluetoothGatt'))
    //     {
    //       console.log(className);
    //     }
    //   },
    //   onComplete: () => console.log('done')
    // });

    // return;

    callbacks.installCallbacks();

    this._cli().catch((reason) =>
    {
      console.error(reason.stack);
    });
  }
}

function main()
{
  recv('config', (cfg) =>
  {
    ConfigManager.setConfig(cfg.payload);

    setTimeout(() => Java.perform(() => new Main()), 0);
  });
}

main();