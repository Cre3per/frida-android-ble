export class Diff
{
  /**
   * @return An array with a true entry wherever the inputs match.
   */
  static bytes(a: number[], b: number[]): boolean[]
  {
    let short: number = a.length;
    let long: number = b.length;

    if (short > long)
    {
      let tmp = short;
      short = long;
      long = tmp;
    }
    
    let result: boolean[] = (new Array(long)).fill(false);

    for (let i = 0; i < short; ++i)
    {
      result[i] = (a[i] === b[i]);
    }

    return result;
  }
}