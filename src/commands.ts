import { historyManager } from "./history";
import { Data, Device, Characteristic, Service } from "./human";
import { Formatter } from "./formatter";
import { Color } from "./color";
import { Diff } from "./diff";
import { Writer } from "./writer";
import { config, ConfigManager } from "./config";

class Command
{
  /**
   * Alias list.
   */
  public alias!: string[];

  /**
   * Usage syntax.
   */
  public usage?: string = '';

  /**
   * 
   */
  public description?: string = '';

  /**
   * 
   */
  public handler!: (args: string[]) => Promise<boolean>;

  constructor(data: Command)
  {
    if (!data.alias || !data.handler)
    {
      console.error('created command without alias or handler.');
    }

    Object.assign(this, data);
  }
}

export class Commands
{
  /**
   * 
   */
  private static _writer = new Writer();

  /**
   * 
   */
  private static readonly _commands = [
    new Command({
      alias: ['diff', 'd'],
      usage: 'diff [type] [id] [id]',
      description: 'diffs the specified entries of a history',
      handler: Commands._diff,
    }),
    new Command({
      alias: ['help', '?', 'commands'],
      usage: 'help',
      description: 'shows available commands',
      handler: Commands._help,
    }),
    new Command({
      alias: ['history', 'h'],
      usage: 'history [type]',
      description: 'displays the specified history. entries can be used with \'diff\'',
      handler: Commands._history,
    }),
    new Command({
      alias: ['listCharacteristics', 'lc'],
      usage: 'listCharacteristics',
      description: 'lists all active characteristics',
      handler: Commands._listCharacteristics,
    }),
    new Command({
      alias: ['searchHistory', 'search', 'find', 's', 'f'],
      usage: 'searchHistory [history] [regexp]',
      description: 'lists all entries in the specified history matching a regular expression',
      handler: Commands._searchHistory,
    }),
    new Command({
      alias: ['writeCharacteristic', 'wc', 'w'],
      usage: 'writeCharacteristic [id] [data]',
      description: 'writes data the the specified characteristic',
      handler: Commands._writeCharacteristic,
    }),
  ];

  /**
   * 
   */
  private static async _help(): Promise<boolean>
  {
    for (let command of this._commands)
    {
      console.log(`${command.usage} - ${command.description}`);
    }

    console.log('or a config key chain and value, eg. "callbacks[android.bluetooth.BluetoothGatt].getService true"');

    return true;
  }

  /**
   * 
   */
  private static _printByteDiff(a: number[], b: number[], diff: boolean[]): void
  {
    let minLength = Math.min(a.length, b.length);

    const generateDiffLine = (source: number[]) =>
    {
      let line = '';

      for (let i = 0; i < diff.length; ++i)
      {
        if (i < source.length)
        {
          let byte = Formatter.byteToString(source[i]);

          if (diff[i])
          {
            line += byte;
          }
          else
          {
            if (i >= minLength)
            {
              // Won't be colored if config.color is disabled, but different
              // lengths don't need highlighting.
              line += Color.PURPLE(byte);
            }
            else
            {
              if (config.color)
              {
                line += Color.RED(byte);
              }
              else
              {
                line += `(${byte})`;
              }
            }
          }

          if (i < (source.length - 1))
          {
            line += ' ';
          }
        }
      }

      return line;
    };

    console.log(generateDiffLine(a));
    console.log(generateDiffLine(b));
  }

  /**
   *
   */
  private static async _diff(args: string[]): Promise<boolean>
  {
    if (args.length === 3)
    {
      let historyType = historyManager.getHistoryByName(args[0]);

      if (historyType !== undefined)
      {
        let a = historyManager.getBytesAt(historyType, parseInt(args[1]));
        let b = historyManager.getBytesAt(historyType, parseInt(args[2]));

        if (a && b)
        {
          this._printByteDiff(a, b, Diff.bytes(a, b));
          return true;
        }
      }
    }

    return false;
  }

  /**
   * 
   */
  public static async _history(args: string[]): Promise<boolean>
  {
    if (args.length === 1)
    {
      let historyType = historyManager.getHistoryByName(args[0]);

      if (historyType !== undefined)
      {
        let bytes = historyManager.getBytes(historyType);

        console.log(`${bytes.length} entries in history ${args[0]}`);

        for (let entry of bytes)
        {
          let humanData = new Data(null, entry.data);

          console.log(`${entry.index}] ${humanData.getDisplayName()}`);
        }

        return true;
      }
    }

    return false;
  }

  /**
   * 
   */
  private static async _listCharacteristics(args: string[]): Promise<boolean>
  {
    if (args.length !== 0)
    {
      return false;
    }

    await this._writer.update();
    let gatts = await this._writer.getGatts();

    Java.perform(() =>
    {
      for (let gatt of gatts)
      {
        let humanDevice = new Device(gatt.handle.getDevice());

        console.log(humanDevice.getDisplayName());

        for (let service of gatt.services)
        {
          let humanService = new Service(service.handle);

          console.log('  ' + humanService.getDisplayName());

          for (let characteristic of service.characteristics)
          {
            let humanCharacteristic = new Characteristic(characteristic.handle);

            console.log('  ' + '  ' + characteristic.id + '] ' + humanCharacteristic.getDisplayName());
          }
        }
      }
    });

    return true;
  }

  /**
   * 
   */
  private static _highlightRegExpMatches(str: string, matches: RegExpExecArray[]): string
  {
    let result: string = '';

    let nextStart = 0;

    for (let match of matches)
    {
      result += str.substring(nextStart, match.index);

      if (config.color)
      {
        result += Color.yellow(match[0]);
      }
      else
      {
        result += `(${match[0]})`;
      }

      nextStart = (match.index + match[0].length);
    }

    result += str.substr(nextStart);

    return result;
  }

  /**
   * 
   */
  private static async _searchHistory(args: string[]): Promise<boolean>
  {
    if (args.length === 2)
    {
      let historyName = args[0];
      let historyType = historyManager.getHistoryByName(historyName);

      if (historyType !== undefined)
      {
        if (args[1][0] !== '/')
        {
          args[1] = `/${args[1]}/g`;
        }

        let flagsStart = args[1].lastIndexOf('/');

        let regex = args[1].substring(1, flagsStart);
        let flags = args[1].substr(flagsStart + 1);

        if (!flags.includes('g'))
        {
          flags += 'g';
        }

        let pattern: RegExp;

        try
        {
          pattern = new RegExp(regex, flags);
        }
        catch
        {
          return false;
        }

        let history = historyManager.getBytes(historyType);

        for (let entry of history)
        {
          let strData = Formatter.byteArrayToString(entry.data);

          let matches = strData.matchAll(pattern);

          if (matches.length > 0)
          {
            // We can't resolve to a custom name, because we'd need the
            // characteristic's handle, which we didn't store.
            console.log(`[${historyName} ${entry.index}] ${this._highlightRegExpMatches(strData, matches)}`);
          }
        }

        return true;
      }
    }

    return false;
  }

  /**
   * 
   */
  private static async _writeCharacteristic(args: string[]): Promise<boolean>
  {
    if (args.length === 2)
    {
      return new Promise(async (resolve) =>
      {
        let bytes = Formatter.stringToByteArray(args[1]);

        if (bytes)
        {
          let characteristicId: (string | number);

          if (!(Formatter.isDecimalNumber(args[0]) && (characteristicId = parseInt(args[0]), !isNaN(characteristicId))))
          {
            characteristicId = args[0];
          }

          if (!await this._writer.writeCharacteristic(characteristicId, bytes))
          {
            console.log('write failed');
          }

          resolve(true);
        }
        else
        {
          resolve(false);
        }
      });
    }

    return false;
  }

  /**
   * 
   */
  public static async onCommand(command: string, args: string[]): Promise<void>
  {
    let cmd = this._commands.find(el => el.alias.includes(command));

    if (cmd)
    {
      if (!await cmd.handler.call(this, args))
      {
        console.log(cmd.usage);
      }
    }
    else
    {
      if ((args.length === 1) && (ConfigManager.set(command, args[0])))
      {
        // all good
      }
      else
      {
        console.log(`${command} was not recognized as a command or config entry. see 'help' for a list of commands.`);
      }
    }
  }

  /**
   * Sends all aliases to the host.
   */
  public static exposeAliases(): void
  {
    let aliases: string = '';

    for (let command of this._commands)
    {
      for (let alias of command.alias)
      {
        aliases += alias;
        aliases += ',';
      }
    }

    // Remove trailing comma.
    aliases = aliases.substr(0, aliases.length - 1);

    send(`aliases:${aliases}`);
  }
}