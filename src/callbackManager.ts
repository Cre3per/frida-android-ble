import { historyManager, HistoryType } from './history.js';
import { Logger } from './logger.js';
import { Characteristic, Data, Device, Service } from './human.js';
import { config, ConfigManager } from './config.js';
import { Formatter } from './formatter.js';

export interface CallbackContext
{
  this: any;
  retval: any;
}

/**
 * {@link https://github.com/pandalion98/SPay-inner-workings/blob/master/boot.oat_files/arm64/dex/out0/android/bluetooth/IBluetoothGatt.java}
 */
class IBluetoothGatt$Stub$Proxy
{
  private readonly _className = 'IBluetoothGatt$Stub$Proxy';

  /**
   * @param {primitive number} clientIf
   * @param {java.lang.String} address
   * @param {primitive number} srvcType
   * @param {primitive number} srvcInstance
   * @param unknown srvcInstanceId, srvcId, charInstanceId, charId, writeType, authReq
   * @param {primitive byte[]} clientIf
   */
  public writeCharacteristic(context: CallbackContext,
    clientIf: any,
    address: any,
    srvcType: any,
    srvcInstance: any,
    unknown: any,
    value: any): void
  {
    let characteristicValue = value;

    let humanDevice = new Device(address);
    let humanData = new Data(undefined, characteristicValue);

    let historyId = historyManager.pushBytes(HistoryType.WRITE, Formatter.toJSArray(characteristicValue));

    Logger.logFunctionCall(`${this._className}.writeCharacteristic`, {
      'clientIf': clientIf,
      'address': humanDevice.getDisplayName(),
      'srvcType': srvcType, 
      'srvcInstance': srvcInstance,
      'a5': unknown,
      'data': `[${historyManager.getHistoryName(HistoryType.WRITE)} ${historyId}] ${humanData.getDisplayName()}`,
    });
  }

  /**
   * 
   */
  constructor()
  {

  }
}

class BluetoothGatt
{
  private readonly _className = 'BluetoothGatt';

  /**
   * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
   */
  public writeCharacteristic(context: CallbackContext, characteristic: any): void
  {
    callbacks.enabled = false;
    let characteristicValue = characteristic.getValue();
    callbacks.enabled = true;

    let humanDevice = new Device(context.this.getDevice());
    let humanCharacteristic = new Characteristic(characteristic);
    let humanService = new Service(characteristic.getService());
    let humanData = new Data(characteristic, characteristicValue);

    let historyId = historyManager.pushBytes(HistoryType.WRITE, Formatter.toJSArray(characteristicValue));

    Logger.logFunctionCall(`${this._className}.writeCharacteristic`, {
      'device': humanDevice.getDisplayName(),
      'service': humanService.getDisplayName(),
      'characteristic': humanCharacteristic.getDisplayName(),
      'data': `[${historyManager.getHistoryName(HistoryType.WRITE)} ${historyId}] ${humanData.getDisplayName()}`,
    });
  }

  /**
   * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
   */
  public readCharacteristic(context: CallbackContext, characteristic: any): void
  {
    callbacks.enabled = false;
    let characteristicValue = characteristic.getValue();
    callbacks.enabled = true;

    let humanDevice = new Device(context.this.getDevice());
    let humanCharacteristic = new Characteristic(characteristic);
    let humanService = new Service(characteristic.getService());
    let humanData = new Data(characteristic, characteristicValue);

    Logger.logFunctionCall(`${this._className}.readCharacteristic`, {
      'device': humanDevice.getDisplayName(),
      'service': humanService.getDisplayName(),
      'characteristic': humanCharacteristic.getDisplayName(),
      'data': humanData.getDisplayName(),
    });
  }

  /**
   * @param {android.bluetooth.BluetoothGattDescriptor} descriptor
   */
  writeDescriptor(context: CallbackContext, descriptor: any): void
  {
    let device = context.this.getDevice();
    let characteristic = descriptor.getCharacteristic();

    let humanDevice = new Device(device);
    let humanCharacteristic = new Characteristic(characteristic);
    let humanService = new Service(characteristic ? characteristic.getService() : null);
    let humanData = new Data(characteristic, descriptor.getValue());

    let historyId = historyManager.pushBytes(HistoryType.WRITE, Formatter.toJSArray(descriptor.getValue()));

    Logger.logFunctionCall(`${this._className}.writeDescriptor`, {
      'device': humanDevice.getDisplayName(),
      'service': humanService.getDisplayName(),
      'characteristic': humanCharacteristic.getDisplayName(),
      'data': `[${historyManager.getHistoryName(HistoryType.WRITE)} ${historyId}] ${humanData.getDisplayName()}`,
    });
  }

  /**
   * 
   */
  constructor()
  {

  }
}

class BluetoothGattCallback
{
  private readonly _className = 'BluetoothGattCallback';

  /**
   * @param {android.bluetooth.BluetoothGatt} gatt
   * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
   */
  public onCharacteristicChanged(context: CallbackContext, gatt: any, characteristic: any): void
  {
    callbacks.enabled = false;
    let value = characteristic.getValue();
    callbacks.enabled = true;

    let humanCharacteristic = new Characteristic(characteristic);
    let humanService = new Service(characteristic.getService());
    let humanData = new Data(characteristic, value);

    let historyId = historyManager.pushBytes(HistoryType.READ, Formatter.toJSArray(value));

    Logger.logFunctionCall(`${this._className}.onCharacteristicChanged`, {
      'service': humanService.getDisplayName(),
      'characteristic': humanCharacteristic.getDisplayName(),
      'data': `[${historyManager.getHistoryName(HistoryType.READ)} ${historyId}] ${humanData.getDisplayName()}`,
    });
  }
}

class BluetoothGattCharacteristic
{
  private readonly _className = 'BluetoothGattCharacteristic';

  /**
   * 
   */
  public getValue(context: CallbackContext): void
  {
    let humanCharacteristic = new Characteristic(context.this);
    let humanService = new Service(context.this.getService());
    let humanData = new Data(context.this, context.retval);

    let historyId = historyManager.pushBytes(HistoryType.READ, Formatter.toJSArray(context.retval));

    if (config.debug_on_characteristic_changed)
    {
      Java.perform(() =>
      {
        // I'm sure there's a nicer way, but this is for debugging only and I
        // don't like Java.
        let trace: string = Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new());

        let lines = trace.split('\n');

        if (lines.length >= 3)
        {
          if (lines[2].includes('.onCharacteristicChanged('))
          {
            console.log(`${this._className}.getValue called`, lines[2].trim());
          }
        }
      });
    }

    Logger.logFunctionCall(`${this._className}.getValue`, {
      'service': humanService.getDisplayName(),
      'characteristic': humanCharacteristic.getDisplayName(),
      'data': `[${historyManager.getHistoryName(HistoryType.READ)} ${historyId}] ${humanData.getDisplayName()}`,
    });
  }
}

interface CallbackManager
{
  [key: string]: any;
}

class CallbackManager
{
  /**
   * Maps function addresses to the original function implementation.
   */
  private _originalImplementations: { [key: string]: any } = {};

  /**
   * Enable hooks. Used to prevent recursive and internal calls from within
   * hooks.
   */
  public enabled: boolean = true;

  /**
   * 
   */
  private _getFunctionId(javaFunction: any): string
  {
    return javaFunction.handle.toString();
  }

  /**
   * Returns a list of alternative class names for the class @className.
   * Callbacks are installed under the @className name, even when they actually
   * hook a class of a different name.
   */
  private _findAlternativeClasses(className: string): string[]
  {
    switch (className)
    {
      case 'android.bluetooth.BluetoothGattCallback':
        return [
          'no.nordicsemi.android.ble.MainThreadBluetoothGattCallback'
        ];
      default:
        return [];
    }
  }

  /**
   * @return {MethodDispatcher}
   */
  private _getJavaFunctionByName(className: string, functionName: string): (undefined | any)
  {
    let javaClass: any;

    try
    {
      javaClass = Java.use(className);
    }
    catch
    {
      return undefined;
    }

    return javaClass[functionName];
  }

  /**
   * 
   */
  private _installCallback(javaFunction: any, callback: any)
  {
    let _this = this;

    javaFunction.implementation = function (...args: any)
    {
      let result = javaFunction.callWrapped.call(this, ...args);

      if (_this.enabled)
      {
        callback({
          this: this,
          retval: result,
        }, ...args);
      }

      return result;
    };
  }

  /**
   * 
   */
  private _enableOverloadedCallback(
    javaFunction: any,
    callbackClassName: string,
    functionName: string,
    forceGeneric: boolean): void
  {
    let functionId: string = this._getFunctionId(javaFunction);

    this._originalImplementations[functionId] = javaFunction.implementation;

    let callback: (...args: any) => void;

    if (!forceGeneric && this[callbackClassName] && this[callbackClassName][functionName])
    {
      callback = (...args) =>
      {
        this[callbackClassName][functionName](...args);
      }
    }
    else // No rich callback available or desired.
    {
      callback = (context: CallbackContext, ...args) =>
      {
        this.genericCallback(`${callbackClassName}.${functionName}`, context, ...args);
      };
    }

    this._installCallback(javaFunction, callback);
  }

  /**
   * Enables a hook.
   */
  private _enableCallback(
    className: string,
    functionName: string,
    callbackClassName: string,
    forceGeneric: boolean): void
  {
    let javaFunction: any = this._getJavaFunctionByName(className, functionName);

    if (!javaFunction)
    {
      return;
    }

    for (let overload of javaFunction.overloads)
    {
      // javaFunction(...) doesn't work, we need a wrapper.
      overload.callWrapped = function (...args: any)
      {
        return this[functionName](...args);
      }

      this._enableOverloadedCallback(overload, callbackClassName, functionName, forceGeneric);
    }
  }

  /**
   * 
   */
  private _disableOverloadedCallback(javaFunction: any): void
  {
    let functionId: string = this._getFunctionId(javaFunction);
    let originalImplementation = this._originalImplementations[functionId];

    javaFunction.implementation = originalImplementation;

    delete this._originalImplementations[functionId];
  }

  /**
   * Enables a hook and all of its alternative hooks.
   * @param className Class parenting the function.
   * @param functionName Function to be hooked.
   * @param forceGeneric Use the generic callback, even if a rich one exists.
   */
  public enableCallback(className: string, functionName: string, forceGeneric: boolean = false): void
  {
    Java.perform(() =>
    {
      let classNames = [className, ...this._findAlternativeClasses(className)];

      for (let _className of classNames)
      {
        this._enableCallback(_className, functionName, className, forceGeneric);
      }
    });
  }

  /**
   * Disables a hook.
   */
  public disableCallback(className: string, functionName: string): void
  {
    Java.perform(() =>
    {
      let javaFunction = this._getJavaFunctionByName(className, functionName);

      if (!javaFunction)
      {
        console.log(`${className}.${functionName} doesn't exist`);
        return;
      }

      for (let overload of javaFunction.overloads)
      {
        this._disableOverloadedCallback(overload);
      }
    });
  }

  /**
   * Toggles a hook.
   */
  public toggleCallback(className: string, functionName: string): void
  {
    Java.perform(() =>
    {
      let javaFunction = this._getJavaFunctionByName(className, functionName);

      if (javaFunction === undefined)
      {
        console.log(`${className}.${functionName} doesn't exist.`);
        return;
      }

      let functionId = this._originalImplementations[this._getFunctionId(javaFunction)];

      if (functionId === undefined)
      {
        this.enableCallback(className, functionName);
      }
      else
      {
        this.disableCallback(className, functionName);
      }
    });
  }

  /**
   * Installs Java hooks.
   */
  public installCallbacks()
  {
    for (let className in config.callbacks)
    {
      for (let functionName in config.callbacks[className])
      {
        if (config.callbacks[className][functionName])
        {
          this.enableCallback(className, functionName, config.callbacks[className][functionName] === 'generic');
        }
      }
    }
  }

  /**
   * Used for functions without a custom callback implementation.
   */
  public genericCallback(name: string, context: CallbackContext, ...args: any): void
  {
    const javaObject = Java.use('java.lang.Object');

    let logArgs: { [key: string]: any } = {};

    let i = 0;
    for (let arg of args)
    {
      let argString: string;

      try
      {
        argString = Java.cast(arg, javaObject).toString();
      }
      catch
      {
        argString = `${arg}`;
      }

      // HACKHACK: Key-Value order is undefined in JS. We could fix this by
      // HACKHACK: rewriting logFunctionCall to take an array rather than an
      // HACKHACK: object.
      logArgs[`a${i++}`] = argString;
    }

    Logger.logFunctionCall(name, logArgs);
  }

  /**
   * 
   */
  constructor()
  {
    this['android.bluetooth.IBluetoothGatt$Stub$Proxy'] = new IBluetoothGatt$Stub$Proxy();

    this['android.bluetooth.BluetoothGatt'] = new BluetoothGatt();
    this['android.bluetooth.BluetoothGattCallback'] = new BluetoothGattCallback();
    this['android.bluetooth.BluetoothGattCharacteristic'] = new BluetoothGattCharacteristic();

    ConfigManager.attachChangeListener('callbacks',
      (keyChain: string[], oldValue: boolean, newValue: boolean) =>
      {
        // We're safe to assume that @keyChain refers to a valid callback.
        let className = keyChain[1];
        let functionName = keyChain[keyChain.length - 1];

        this.toggleCallback(className, functionName);
      });
  }
}

export let callbacks = new CallbackManager();