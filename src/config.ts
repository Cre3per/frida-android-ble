export let config: any = {};

type ConfigChangeListener = (keyChain: string[], oldValue: any, newValue: any) => void;

interface ConfigChangeListenerGroup
{
  keyChain: string[];
  listeners: ConfigChangeListener[];
}

export class ConfigManager
{
  /**
   * Config change listeners.
   */
  private static _listeners: ConfigChangeListenerGroup[] = [];

  /**
   * All resolve calls waiting for the config to be loaded.
   */
  private static _loadedCallbacks: (() => void)[] = [];

  /**
   * Used to wait until the config has been loaded.
   */
  private static _configLoaded(): Promise<void>
  {
    return new Promise((resolve) =>
    {
      if (Object.keys(config).length > 0)
      {
        resolve();
      }
      else
      {
        this._loadedCallbacks.push(resolve);
      }
    });
  }

  /**
   * Converts a key chain string to a key chain array.
   * Key chain strings are separated by dots and brackets.
   * 
   * eg.
   * callbacks[android.bluetooth.BluetoothGatt].writeCharacteristic
   * turns into
   * ['callbacks', 'android.bluetooth.BluetoothGatt', 'writeCharacteristic']
   */
  private static _keyChainFromString(keyChainString: string): string[]
  {
    let keyChain: string[] = [];
    let bracket: boolean = false;

    let key = '';

    const fnKeyEnd = () =>
    {
      if (key.length > 0)
      {
        keyChain.push(key);
        key = '';
      }
      else
      {
        throw new Error(`keys cannot be empty, in ${keyChainString}`);
      }
    };

    for (let c of keyChainString)
    {
      switch (c)
      {
        case ']':
          if (bracket)
          {
            bracket = false;
          }
          else
          {
            throw new Error(`bracket closed but not opened in ${keyChainString}`);
          }
          break;
        case '.':
          if (bracket)
          {
            key += c;
          }
          else
          {
            fnKeyEnd();
          }
          break;
        case '[':
          bracket = true;
          fnKeyEnd();
          break;
        default:
          key += c;
          break;
      }
    }

    if (bracket)
    {
      throw new Error(`bracket opened but not closed in ${keyChainString}`);
    }

    if (key.length > 0)
    {
      keyChain.push(key);
    }
    else
    {
      throw new Error(`${keyChainString} cannot end in a period`);
    }

    return keyChain;
  }

  /**
   * Turns regex strings into {@link RegExp} objects.
   */
  private static _unstringifyConfig(config: any)
  {
    for (let characteristicUuid in config.known_data)
    {
      let characteristic = config.known_data[characteristicUuid];

      for (let data of characteristic)
      {
        let flagsStart = data.match.lastIndexOf('/');
        let regex = data.match.substring(1, flagsStart);
        let flags = data.match.substr(flagsStart + 1);

        data.match = new RegExp(regex, flags);
      }
    }

    return config;
  };

  /**
   * @return Parent object of the trailing key and the trailing key.
   */
  private static _get(keyChain: string[]): (undefined | {
    parent: { [key: string]: any },
    key: string,
  })
  {
    let parent = config;
    let obj = config;

    for (let key of keyChain)
    {
      if (key in obj)
      {
        parent = obj;
        obj = obj[key];
      }
      else
      {
        return undefined;
      }
    }

    return {
      parent: parent,
      key: keyChain[keyChain.length - 1]
    };
  }

  /**
   * @param value Will be {@link JSON.parse}d
   * @return The old value of the config entry or undefined if @keyChain is
   *         invalid.
   */
  private static _set(keyChain: string[], value: string): (undefined | any)
  {
    let entry = this._get(keyChain);

    if (entry === undefined)
    {
      return undefined;
    }
    else
    {
      let jsValue: any;

      try
      {
        jsValue = JSON.parse(value);
      }
      catch
      {
        console.warn(`invalid json value: ${value}`);
        return undefined;
      }

      // This is a very basic check, it's not safe.
      if (typeof (entry.parent[entry.key]) === typeof (jsValue))
      {
        let oldValue = entry.parent[entry.key];

        entry.parent[entry.key] = jsValue;

        return oldValue;
      }
      else
      {
        console.warn(`invalid value type ${typeof (value)}, expected ${typeof (entry.parent[entry.key])}`);
        return undefined;
      }
    }
  }

  /**
   * 
   */
  private static _getListeners(keyChain: string[]): (undefined | ConfigChangeListenerGroup)
  {
    return this._listeners.find(listener => listener.keyChain.every((el, index) => (keyChain[index] === el)));
  }

  /**
   * Invokes listeners on @keyChainListener but not on sub key chains.
   * @param keyChainListener The key chain the listener is attached to.
   * @param keyChainTarget The key chain that was updated.
   */
  private static _invokeListenersImpl(
    keyChainListener: string[], keyChainTarget: string[],
    oldValue: any, newValue: any): void
  {
    for (let listener of this._listeners)
    {
      if (listener.keyChain.length !== keyChainListener.length)
      {
        continue;
      }

      if (this._getListeners(keyChainListener) !== undefined)
      {
        for (let callback of listener.listeners)
        {
          callback(keyChainTarget, oldValue, newValue);
        }

        break;
      }
    }
  }

  /**
   * Invokes listeners on @keyChain and all sub-keychains.
   */
  private static _invokeListeners(keyChain: string[], oldValue: any, newValue: any): void
  {
    if (keyChain.length > 0)
    {
      let subChain: string[] = [];

      for (let key of keyChain)
      {
        subChain.push(key);

        this._invokeListenersImpl(subChain, keyChain, oldValue, newValue);
      }
    }
  }

  /**
   *
   */
  public static set(keyChainString: string, value: string): boolean
  {
    let keyChain: string[];

    try
    {
      keyChain = this._keyChainFromString(keyChainString);
    }
    catch (ex)
    {
      // {@link #set} is called without knowing if the user's intention is to
      // access the config.
      // console.log(ex);

      return false;
    }

    let oldValue = this._set(keyChain, value);

    if (oldValue !== undefined)
    {
      this._invokeListeners(keyChain, oldValue, value);

      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Attaches a config change listener to a value or an object.
   */
  public static async attachChangeListener(keyChainString: string, listener: ConfigChangeListener): Promise<void>
  {
    await this._configLoaded();

    let keyChain: string[] = this._keyChainFromString(keyChainString);

    if (this._get(keyChain) === undefined)
    {
      console.error(`tried to attach a listener to ${keyChain}, which doesn't exist`);
      return;
    }

    let listeners = this._getListeners(keyChain);

    if (listeners === undefined)
    {
      this._listeners.push({
        keyChain: keyChain,
        listeners: [listener],
      });
    }
    else
    {
      listeners.listeners.push(listener);
    }
  }

  /**
   * 
   */
  public static setConfig(receivedConfig: any)
  {
    Object.assign(config, this._unstringifyConfig(receivedConfig));

    this._loadedCallbacks.forEach(el => el());
  }
}