import { config, ConfigManager } from "./config";

export enum HistoryType
{
  READ,
  WRITE,

  COUNT
}

export class HistoryEntry<T>
{
  public index: number;
  public data: T;

  constructor(index: number, data: T)
  {
    this.index = index;
    this.data = data;
  }
}

export interface History<T>
{
  type: HistoryType;
  name: string;
  history: HistoryEntry<T>[];
  nextIndex: number;
}

class HistoryManager
{
  private _histories: History<any>[] = [];


  /**
   * @return History of type @type
   */
  public getHistory<T>(type: HistoryType): History<T>
  {
    return this._histories.find(el => (el.type === type))!;
  }

  /**
   * 
   */
  public getHistoryByName(name: string): (undefined | HistoryType)
  {
    let found = this._histories.find(el => (el.name === name));

    if (found)
    {
      return found.type;
    }
    else
    {
      return undefined;
    }
  }

  /**
   * 
   */
  public getHistoryName(history: HistoryType): string
  {
    return this.getHistory(history).name;
  }

  /**
   * @return Index of the pushed entry.
   */
  public pushBytes(historyType: HistoryType, data: number[]): number
  {
    if (config.history_size > 0)
    {
      let history = this.getHistory<number[]>(historyType)!;

      if (history.history.length >= config.history_size)
      {
        history.history.splice(0, (config.history_size - history.history.length) + 1);
      }

      return (history.history.push({
        index: history.nextIndex++,
        data: data
      }) - 1);
    }

    return 0;
  }

  /**
   * @return Byte history.
   */
  public getBytes(historyType: HistoryType): HistoryEntry<number[]>[]
  {
    return this.getHistory<number[]>(historyType).history;
  }

  /**
   * @return Byte diff entry at @index.
   */
  public getBytesAt(historyType: HistoryType, index: number): (undefined | number[])
  {
    let history = this.getHistory<number[]>(historyType).history;

    for (let el of history)
    {
      if (el.index === index)
      {
        return el.data;
      }
    }

    return undefined;
  }


  constructor()
  {
    let types = [
      {
        type: HistoryType.READ,
        name: 'read',
      },
      {
        type: HistoryType.WRITE,
        name: 'write',
      },
    ];

    for (let history of types)
    {
      this._histories.push({
        type: history.type,
        name: history.name,
        history: [],
        nextIndex: 0,
      });
    }

    if (this._histories.length !== HistoryType.COUNT)
    {
      console.error(`this._histories.length (${this._histories.length}) !== HistoryType.length (${Object.keys(HistoryType).length})`);
    }
  }
}

export let historyManager = new HistoryManager();