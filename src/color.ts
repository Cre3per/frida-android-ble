import { config } from './config.js';

export class Color
{
  /**
   * @link{https://stackoverflow.com/a/33206814/9364954}
   */
  private static convert(str: string, effects: number[]): string
  {
    if (config.color)
    {
      let result = `\x1b[`;

      for (let i = 0; i < effects.length; ++i)
      {
        result += effects[i];

        if (i !== (effects.length - 1))
        {
          result += ';';
        }
      }

      result += 'm';

      result += str;

      result += '\x1b[0m';

      return result;
    }
    else
    {
      return str;
    }
  }

  public static red(str: string): string
  {
    return this.convert(str, [30 + 1]);
  }

  public static RED(str: string): string
  {
    return this.convert(str, [40 + 1]);
  }

  public static yellow(str: string): string
  {
    return this.convert(str, [30 + 3]);
  }

  public static YELLOW(str: string): string
  {
    return this.convert(str, [40 + 3]);
  }

  public static purple(str: string): string
  {
    return this.convert(str, [30 + 5]);
  }

  public static PURPLE(str: string): string
  {
    return this.convert(str, [40 + 5]);
  }
}