export class Logger
{
  private static _getHumanTime(): string
  {
    const makeString = (i: number): string =>
    {
      let str = i.toString();

      if (i < 10)
      {
        str = '0' + str;
      }

      return str;
    };

    let date = new Date();

    return `${date.getFullYear()}-${makeString(date.getMonth() + 1)}-${makeString(date.getDay())}` +
      ` ${makeString(date.getHours())}:${makeString(date.getMinutes())}:${makeString(date.getSeconds())}`;
  }

  /**
   * @param {string} functionName
   * @param {Object} args
   */
  public static logFunctionCall(functionName: string, args: { [key: string]: any })
  {
    console.log(`[${this._getHumanTime()}] ${functionName}(${args ? '' : ')'}`);

    if (args)
    {
      for (let key of Object.keys(args))
      {
        console.log(`  ${key}: ${args[key]}`);
      }

      console.log(')');
    }
  }
}