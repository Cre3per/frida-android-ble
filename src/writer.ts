interface IndexedHandle
{
  id: number;
  handle: any;
}

type WriterCharacteristic = IndexedHandle;

interface WriterService extends IndexedHandle
{
  characteristics: WriterCharacteristic[];
}

interface WriterGatt extends IndexedHandle
{
  services: WriterService[];
}


export class Writer
{
  private _gatts: WriterGatt[] = [];

  /**
   * 
   */
  private async _collectInstances(className: string): Promise<IndexedHandle[]>
  {
    let result: IndexedHandle[] = [];

    return new Promise((resolve) =>
    {
      Java.perform(() =>
      {
        Java.choose(className, {
          onMatch: (instance: any) =>
          {
            result.push({
              id: result.length,
              handle: Java.retain(instance),
            });
          },
          onComplete: () =>
          {
            resolve(result);
          }
        });
      });
    });
  }

  /**
   * 
   */
  private async _collectGatts(): Promise<IndexedHandle[]>
  {
    return this._collectInstances('android.bluetooth.BluetoothGatt');
  }

  /**
   * @return The characteristic's id.
   */
  private async _getCharacteristicByUuid(uuid: string): Promise<undefined | number>
  {
    return new Promise((resolve) =>
    {
      Java.perform(() =>
      {
        for (let gatt of this._gatts)
        {
          for (let service of gatt.services)
          {
            for (let characteristic of service.characteristics)
            {
              if (characteristic.handle.getUuid().toString() === uuid)
              {
                resolve(characteristic.id);
              }
            }
          }
        }

        resolve(undefined);
      });
    });
  }

  /**
   * 
   */
  private _getCharacteristicPath(id: number): (undefined | {
    gatt: WriterGatt,
    service: WriterService,
    characteristic: WriterCharacteristic,
  })
  {
    for (let gatt of this._gatts)
    {
      for (let service of gatt.services)
      {
        for (let characteristic of service.characteristics)
        {
          if (characteristic.id === id)
          {
            return {
              gatt,
              service,
              characteristic
            };
          }
        }
      }
    }

    return undefined;
  }

  /**
   * {@link update} has to be called first.
   */
  public getGatts(): WriterGatt[]
  {
    return this._gatts;
  }

  /**
   * @param characteristic Characteristic id or uuid.
   */
  public async writeCharacteristic(characteristicId: (string | number), value: number[]): Promise<boolean>
  {
    if (this._gatts.length === 0)
    {
      await this.update();
    }

    if (typeof(characteristicId) === 'string')
    {
      let internalId = await this._getCharacteristicByUuid(characteristicId);

      if (internalId === undefined)
      {
        console.log(`unknown uuid`);
        return false;
      }
      else
      {
        characteristicId = internalId;
      }
    }

    let characteristicPath = this._getCharacteristicPath(<number> characteristicId);

    if (characteristicPath)
    {
      return new Promise((resolve) =>
      {
        Java.perform(() =>
        {
          characteristicPath = characteristicPath!;

          let characteristic = characteristicPath.characteristic;

          if (characteristic.handle.setValue(Java.array('byte', value)))
          {
            resolve(characteristicPath.gatt.handle.writeCharacteristic(characteristic.handle));
          }
          else
          {
            console.log('characteristic.setValue failed');
            resolve(false);
          }
        });
      });
    }
    else
    {
      console.warn(`called writeCharacteristic on an invalid device or characteristic`);
      return false;
    }
  }

  /**
   * Gets all active characteristics and stores them locally.
   * Requires that android.bluetooth.BluetoothGatt.discoverServices has already
   * been called on the gatt instances.
   * @return The found characteristics.
   */
  public async update(): Promise<void>
  {
    // should we Java.retain in here?

    this._gatts = [];

    let nextGattId = 0;
    let nextServiceId = 0;
    let nextCharacteristicId = 0;

    let gatts = await this._collectGatts();

    return new Promise((resolve) =>
    {
      Java.perform(() =>
      {
        for (let gatt of gatts)
        {
          this._gatts.push({
            id: nextGattId++,
            handle: gatt.handle,
            services: [],
          });
          let mGatt = this._gatts[this._gatts.length - 1];

          // The cast overload of @toArray doesn't work.
          let services = gatt.handle.getServices().toArray();

          let BluetoothGattService = Java.use('android.bluetooth.BluetoothGattService');
          let BluetoothGattCharacteristic = Java.use('android.bluetooth.BluetoothGattCharacteristic');

          for (let service of services)
          {
            let javaService = Java.cast(service, BluetoothGattService);

            mGatt.services.push({
              id: nextServiceId++,
              handle: javaService,
              characteristics: [],
            });
            let mService = mGatt.services[mGatt.services.length - 1];

            let characteristics = javaService.getCharacteristics().toArray();

            for (let characteristic of characteristics)
            {
              mService.characteristics.push({
                id: nextCharacteristicId++,
                handle: Java.cast(characteristic, BluetoothGattCharacteristic),
              });
            }
          }
        }

        resolve();
      });
    });
  }

  constructor()
  {

  }
}