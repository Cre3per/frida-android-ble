export class Formatter
{
  /**
   * Converts a single byte to a hex string.
   */
  static byteToString(byte: number): string
  {
    if (byte < 0)
    {
      byte += 0x100;
    }

    let hex = byte.toString(16);

    if (byte < 0x10)
    {
      return `0${hex}`;
    }
    else
    {
      return hex;
    }
  }

  /**
   * Converts a single byte from string to byte.
   * Requires lower case hex.
   * @return -127 to 128
   */
  static stringToByte(str: string): (undefined | number)
  {
    let byte = parseInt(str, 16);

    if (isNaN(byte) || (byte < 0x00) || (byte > 0xff))
    {
      return undefined;
    }
    else
    {
      if (byte > 0x7f)
      {
        byte -= 0x100;
      }

      return byte;
    }
  }

  /**
   * Converts a byte array to human readable format (lower case hex).
   */
  static byteArrayToString(arr: number[]): string
  {
    let str = '';

    for (let i = 0; i < arr.length; ++i)
    {
      str += this.byteToString(arr[i]);

      if (i !== (arr.length - 1))
      {
        str += ' ';
      }
    }

    return str;
  }

  /**
   * Converts a lower case, space separated, hex string to a byte array.
   * Byte values are -127 to 128. Hex values are 00 to ff.
   */
  static stringToByteArray(str: string): (undefined | number[])
  {
    let result: number[] = [];
    let split: string[] = str.split(' ');

    for (let part of split)
    {
      let byte = this.stringToByte(part);

      if (byte === undefined)
      {
        return undefined;
      }
      else
      {
        result.push(byte);
      }
    }

    return result;
  }

  /**
   * 
   */
  public static isDecimalDigit(char: string): boolean
  {
    return '0123456789'.includes(char);
  }

  /**
   * 
   */
  public static isDecimalNumber(str: string): boolean
  {
    return str.split('').every(this.isDecimalDigit);
  }

  /**
   * Converts a Java array to a js array.
   */
  public static toJSArray<T>(javaArray: any): T[]
  {
    let result: T[] = [];

    for (let el of javaArray)
    {
      result.push(el);
    }

    return result;
  }

  /**
   * Converts a Java string to a js string.
   */
  public static toJSString(javaString: any): string
  {
    if (typeof(javaString) === 'string')
    {
      return javaString;
    }

    let result: string = '';

    let length = javaString.length();

    for (let i = 0; i < length; ++i)
    {
      result += javaString.charAt(i);
    }

    return result;
  }
}