import { config } from './config.js';

import { Formatter } from './formatter.js';

class IHumanizer
{
  getDisplayName(): string
  {
    console.log('IHumanizer.getDisplayName called');
    return 'IHumanizer.getDisplayName';
  }
}

export class Device extends IHumanizer
{
  private _deviceAddress: string;
  private _deviceName: (undefined | string);

  /**
   * @return {string}
   */
  getDisplayName(): string
  {
    if (this._deviceAddress)
    {
      if (config.known_devices[this._deviceAddress])
      {
        return config.known_devices[this._deviceAddress];
      }
      else
      {
        return `${this._deviceAddress} (${this._deviceName})`;
      }
    }
    else
    {
      return 'none';
    }
  }

  /**
   * @param {android.bluetooth.BluetoothDevice|string} device Java device or device name
   */
  constructor(device: any)
  {
    super();

    if (typeof(device) === 'string')
    {
      this._deviceAddress = device;
    }
    else
    {
      this._deviceAddress = Formatter.toJSString(device.getAddress());
      this._deviceName = Formatter.toJSString(device.getName());
    }
  }
}

export class Service extends IHumanizer
{
  private _service: any;

  /**
   * @return {string}
   */
  getDisplayName(): string
  {
    if (this._service)
    {
      let uuid = this._service.getUuid();

      if (uuid)
      {
        let strUuid = uuid.toString();

        if (config.known_services[strUuid])
        {
          return config.known_services[strUuid];
        }
        else
        {
          return strUuid;
        }
      }
      else
      {
        return 'unknown';
      }
    }
    else
    {
      return 'none';
    }
  }

  /**
   * @param {android.bluetooth.BluetoothGattService} service
   */
  constructor(service: any)
  {
    super();

    this._service = service;
  }
}

export class Characteristic extends IHumanizer
{
  private _characteristic: any;

  /**
   * @return {string}
   */
  getDisplayName()
  {
    if (this._characteristic)
    {
      let uuid = this._characteristic.getUuid();

      if (uuid)
      {
        let strUuid = uuid.toString();

        if (config.known_characteristics[strUuid])
        {
          return config.known_characteristics[strUuid];
        }
        else
        {
          return strUuid;
        }
      }
      else
      {
        return 'unknown';
      }
    }
    else
    {
      return 'none';
    }
  }

  /**
   * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
   */
  constructor(characteristic: any)
  {
    super();

    this._characteristic = characteristic;
  }
}

export class Data extends IHumanizer
{
  private _characteristic: any;
  private _data: any;
  private _uuid: (undefined | string);

  /**
   * 
   */
  private _getUuid(): string
  {
    if (!this._uuid)
    {
      this._uuid = this._characteristic.getUuid().toString()
    }

    return this._uuid!;
  }

  /**
   * @return Name set in the config.
   */
  public getCustomName(strData?: string): (undefined | string)
  {
    let uuid = this._getUuid();

    let knownData = config.known_data[uuid];

    if (knownData)
    {
      if (!strData)
      {
        strData = Formatter.byteArrayToString(this._data);
      }

      for (let data of config.known_data[uuid])
      {
        if (data.match.test(strData))
        {
          return data.name;
        }
      }
    }

    return undefined;
  }

  /**
   * @return {string}
   */
  public getDisplayName()
  {
    if (this._data)
    {
      let strData = Formatter.byteArrayToString(this._data);

      if (this._characteristic && this._characteristic.getUuid())
      {        
        let customName = this.getCustomName(strData);

        if (customName)
        {
          return `${customName} (${strData})`;
        }
      }

      return strData;
    }
    else
    {
      return 'none';
    }
  }

  /**
   * @param {android.bluetooth.BluetoothGattCharacteristic} characteristic
   * @param {byte[]} data
   */
  constructor(characteristic: any, data: any)
  {
    super();

    this._characteristic = characteristic;
    this._data = data;
  }
}