interface Array<T>
{
  includes(valueToFind: T, fromIndex?: number): boolean;
}