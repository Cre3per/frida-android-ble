interface String
{
  matchAll(regexp: RegExp): RegExpExecArray[];
}