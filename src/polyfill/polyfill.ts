export let polyfill = () =>
{
  Array.prototype.find = function (predicate: (value: any, index: number, obj: any[]) => unknown, thisArg?: any)
  {
    for (let i = 0; i < this.length; ++i)
    {
      if (predicate.call(thisArg, this[i], i, this))
      {
        return this[i];
      }
    }

    return undefined;
  }

  Array.prototype.fill = function (value: any, start?: number, end?: number)
  {
    if (start === undefined)
    {
      start = 0;
    }

    if (end === undefined)
    {
      end = this.length;
    }

    for (let i = start; i < end; ++i)
    {
      this[i] = value;
    }

    return this;
  };

  Array.prototype.includes = function (valueToFind: any, fromIndex?: number): boolean
  {
    return (this.indexOf(valueToFind) !== -1);
  };

  String.prototype.matchAll = function (regexp: RegExp): RegExpExecArray[]
  {
    let result: RegExpExecArray[] = [];

    let originalLastIndex = regexp.lastIndex;
    
    regexp.lastIndex = 0;

    let str: string = this.valueOf();
    let match;

    while (match = regexp.exec(str))
    {
      result.push(match);
    }

    regexp.lastIndex = originalLastIndex;

    return result;
  }
}